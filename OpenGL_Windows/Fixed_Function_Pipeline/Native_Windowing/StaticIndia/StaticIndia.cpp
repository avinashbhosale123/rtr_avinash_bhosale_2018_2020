#include<stdio.h>
#include<Windows.h>
#include<gl/GLU.h>
#include<gl/GL.h>
#define _USE_MATH_DEFINES 1
#include <math.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Global Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global Variable Declaration
HWND ghwnd;
MONITORINFO mi = { sizeof(MONITORINFO) };
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc;
HGLRC ghrc;
bool gbActiveWindow = false;
bool gbDone = false;
int winWidth;
int winHeight;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int initialize(void);
	void display(void);
	
	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyOGLWindow");
	int iRet = 0;

	//Creating log file
	if (fopen_s(&gbFile, "Log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Failed to Create Log File"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Creation is Successful\n");
	}

	//Code
	//Creating BluePrint of the Window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Registering the Window Class
	RegisterClassEx(&wndclass);

	//Creating the Window in the Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (hwnd != NULL)
	{
		fprintf(gbFile, "Window Created successfully\n");
	}

	//Storing the Window Handle into Global Handle
	ghwnd = hwnd;

	//Initializing OpenGL
	iRet = initialize();

	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed!!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Setting Pixel Format Failed!!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Creating Context Failed!!\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Failed!!\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization is Successful!!\n");
	}

	//Setting the Window to Fullscreen
	dwStyle = GetWindowLong(hwnd, GWL_STYLE);
	GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi);
	SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
	SetWindowPos(hwnd,
		HWND_TOP,
		mi.rcMonitor.left,
		mi.rcMonitor.left,
		mi.rcMonitor.right - mi.rcMonitor.left,
		mi.rcMonitor.bottom - mi.rcMonitor.top,
		SWP_NOZORDER | SWP_FRAMECHANGED);
	winWidth = mi.rcMonitor.right;
	winHeight = mi.rcMonitor.bottom;

	//Showing the Created Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Game Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Call Update Here
			}

			display();
		}
	}

	return((int)msg.wParam);
}

//Callback Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Initialize
int initialize(void)
{
	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initialization of PFD Structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	 }

	ghrc = wglCreateContext(ghdc);

	if (ghdc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	//Set the Color State of OpenGL
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	return 0;
}

//Display
void display(void)
{
	//Function Declaration
	void drawIL(void);
	void drawN(void);
	void drawD(void);
	void drawIR(void);
	void drawA(void);

	//Code
	glViewport(0, 0, (GLsizei)winWidth, (GLsizei)winHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLsizei)winWidth / (GLsizei)winHeight, 0.1f, 100.0f);

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -3.0f);
	
	glLineWidth(3.0f);
	drawIL();
	drawN();
	drawD();
	drawIR();
	drawA();
	
	SwapBuffers(ghdc);
}

//Draw I
void drawIL(void)
{
	//Drawing Left I of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.60f, 0.60f, 0.0f);
	glVertex3f(-0.50f, 0.60f, 0.0f);
	glVertex3f(-0.55f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.55f, -0.60f, 0.0f);
	glVertex3f(-0.60f, -0.60f, 0.0f);
	glVertex3f(-0.50f, -0.60f, 0.0f);
	glEnd();
}

void drawN(void)
{
	//Drawing N of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.40f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.40f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.40f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.20f, -0.60f, 0.0f);
	glVertex3f(-0.20f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.20f, 0.60f, 0.0f);
	glEnd();
}

void drawD(void)
{
	//Drawing D of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.10f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.10f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.15f, 0.60f, 0.00f);
	glVertex3f(0.10f, 0.60f, 0.00f);
	glVertex3f(0.10f, 0.60f, 0.00f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.10f, -0.60f, 0.0f);
	glVertex3f(0.10f, -0.60f, 0.0f);
	glVertex3f(-0.15f, -0.60f, 0.00f);
	glEnd();
}

void drawIR(void)
{
	//Drawing Right I of INDIA
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(0.20f, 0.60f, 0.0f);
	glVertex3f(0.30f, 0.60f, 0.0f);
	glVertex3f(0.25f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.25f, -0.60f, 0.0f);
	glVertex3f(0.20f, -0.60f, 0.0f);
	glVertex3f(0.30f, -0.60f, 0.0f);
	glEnd();
}

void drawA(void)
{
	//Drawing A of INDIA
	glBegin(GL_LINES);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.60f, -0.60f, 0.0f);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(0.50f, 0.60f, 0.0f);
	glVertex3f(0.50f, 0.60f, 0.0f);
	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(0.40f, -0.60f, 0.0f);
	glEnd();

	glTranslatef(0.50f, 0.0f, 0.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(1.00f, 0.60f, 0.20f);
	glVertex3f(-0.048f, 0.02f, 0.0f);
	glVertex3f(0.048f, 0.02f, 0.0f);
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);

	glColor3f(0.07f, 0.53f, 0.027f);
	glVertex3f(-0.052f, -0.02f, 0.0f);
	glVertex3f(0.052f, -0.02f, 0.0f);
	glEnd();
}

//Unintialize
void uninitialize(void)
{
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log File Closed Successfully!!\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}