#include<Windows.h>
#include<stdio.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<math.h> //for cos and sin

//Linking the OpenGL libraries
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

//Global Variables
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
HDC ghdc;
HGLRC ghrc;
FILE *gbFile = NULL;
bool gbDone = false;
bool gbIsFullscreen = false;
bool gbActiveWindow = false;
int winWidth = 800; //Declaring Window Width as a variable instead of a constant to use the resized value of window width for Viewport mapping
int winHeight = 600; //Declaring Window Width as a variable instead of a constant to use the resized value of window height for Viewport mapping

//Macro for PI
#define PI 3.14159265358979323846   // pi

//Global Function
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	int initialize(void);
	void display(void);

	//Variable Declaration
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("My OpenGL Window");
	int iRet;

	//Code
	//Open Log File for Writing
	if (fopen_s(&gbFile, "log.txt", "w"))
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Creation is Successful\n");
	}

	//Creating BluePrint of Window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.lpfnWndProc = WndProc;

	//Registering the Window Class
	RegisterClassEx(&wndclass);

	//Creating the Window in the memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	fprintf(gbFile, "Window Created Successfully\n");

	//Storing the window handle into global window handle
	ghwnd = hwnd;

	//Initialize OpenGL
	iRet = initialize();

	//Error Checking
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing of Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Set Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization Successful!!\n");
	}

	//Show the created Window on Screen
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//placeholder for update
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//CallBack
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void FullscreenToggle(void);
	void resize(int, int);
	void uninitialize(void);

	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			FullscreenToggle();
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle
void FullscreenToggle(void)
{
	//Variable Declaration
	MONITORINFO mi;

	//Code
	//Check if the Window is already Fullscreen or not and Act accordingly
	if (gbIsFullscreen == false)
	{
		mi = { sizeof(MONITORINFO) };

		//Get the Window Style
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			gbIsFullscreen = true;
			ShowCursor(FALSE);
		}

	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE);

		gbIsFullscreen = false;
		ShowCursor(TRUE);
	}
}


//Initialize
int initialize(void)
{
	//Function Declaration
	void resize(int, int);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initialization of Pixel Format Descriptor Structure
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	//Getting the index of Pixel Format Descriptor from OS
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(winWidth, winHeight); //Warm up call for resize

	return 0;
}

//Display
void display(void)
{
	//Variable Declaration
	const int num_points = 1000;
	GLfloat radius, x, y;

	//Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	
	glLineWidth(3.0f);
	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 1.0f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.9f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.8f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.7f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.6f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.5f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.4f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.3f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(0.5f, 0.5f, 0.5f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.2f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(1.0f, 0.50f, 0.0f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < num_points; i++)
	{
		GLfloat angle = (2 * PI * i) / num_points;
		radius = 0.1f;
		x = radius * cos(angle);
		y = radius * sin(angle);
		glColor3f(1.0f, 0.0f, 0.50f);
		glVertex3f(x, y, 0.0f);
	}
	glEnd();

	SwapBuffers(ghdc);
}

//Resize
void resize(int winWidth, int winHeight)
{
	//Code
	if (winHeight == 0)
	{
		winHeight = 1;
	}

	glViewport(0, 0, (GLsizei)winWidth, (GLsizei)winHeight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)winWidth / (GLfloat)winHeight, 0.1, 50.0f);

}

//Uninitialize
void uninitialize(void)
{
	//Code
	if (gbIsFullscreen = true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(true);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log File is closed Successfully!!\n");
		fclose(gbFile);
		gbFile = NULL;
	}

}