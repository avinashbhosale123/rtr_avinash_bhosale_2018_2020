#include<GL/freeglut.h>
#include<gl/GL.h>
#include<gl/GLU.h>

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

GLfloat angle_pyramid = 0.0f;
GLfloat angle_cube = 0.0f;
bool bIsFullscreen = false;

int main(int argc, char *argv[])
{
	//Function Declarations
	void initialize(void);
	void uninitialize(void);
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void update(void);

	//code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First OpenGL Program - Avinash Bhosale");

	initialize();

	//Callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);
	glutIdleFunc(update);

	glutMainLoop();

	return 0;
}

void initialize(void)
{
	//Code
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void uninitialize(void)
{
	//code
}

void display(void)
{
	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -6.0f);
	glRotatef(angle_pyramid, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.60f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.60f, -0.60f, 0.60f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.60f, -0.60f, 0.60f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.60f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.60f, -0.60f, 0.60f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.60f, -0.60f, -0.60f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.60f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.60f, -0.60f, -0.60f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.60f, -0.60f, -0.60f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.60f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-0.60f, -0.60f, -0.60f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.60f, -0.60f, 0.60f);
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(angle_cube, 1.0f, 1.0f, 1.0f);

	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.60f, 0.60f, -0.60f);
	glVertex3f(0.60f, 0.60f, 0.60f);
	glVertex3f(-0.60f, 0.60f, 0.60f);
	glVertex3f(-0.60f, 0.60f, -0.60f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.60f, -0.60f, -0.60f);
	glVertex3f(0.60f, -0.60f, 0.60f);
	glVertex3f(-0.60f, -0.60f, 0.60f);
	glVertex3f(-0.60f, -0.60f, -0.60f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.60f, 0.60f, 0.60f);
	glVertex3f(-0.60f, 0.60f, 0.60f);
	glVertex3f(-0.60f, -0.60f, 0.60f);
	glVertex3f(0.60f, -0.60f, 0.60f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(0.60f, 0.60f, -0.60f);
	glVertex3f(-0.60f, 0.60f, -0.60f);
	glVertex3f(-0.60f, -0.60f, -0.60f);
	glVertex3f(0.60f, -0.60f, -0.60f);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(0.60f, 0.60f, -0.60f);
	glVertex3f(0.60f, 0.60f, 0.60f);
	glVertex3f(0.60f, -0.60f, 0.60f);
	glVertex3f(0.60f, -0.60f, -0.60f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.60f, 0.60f, -0.60f);
	glVertex3f(-0.60f, 0.60f, 0.60f);
	glVertex3f(-0.60f, -0.60f, 0.60f);
	glVertex3f(-0.60f, -0.60f, -0.60f);
	glEnd();


	glutSwapBuffers();
}


void reshape(int winWidth, int winHeight)
{
	if (winHeight == 0)
	{
		winHeight = 1;
	}

	glViewport(0, 0, (GLsizei)winWidth, (GLsizei)winHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)winWidth / (GLfloat)winHeight, 0.1f, 100.0f);
}

void update(void)
{
	angle_pyramid = angle_pyramid + 0.2f;
	if (angle_pyramid >= 360.0f)
	{
		angle_pyramid = 0.0f;
	}

	angle_cube = angle_cube + 0.2f;
	if (angle_cube >= 360.0f)
	{
		angle_cube = 0.0f;
	}

	glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case'F':
	case'f':
		if (bIsFullscreen == false)
		{
			glutFullScreen();
			bIsFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullscreen = false;
		}
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}