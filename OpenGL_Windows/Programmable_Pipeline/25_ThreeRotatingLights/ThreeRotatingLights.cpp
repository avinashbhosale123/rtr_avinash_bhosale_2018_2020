#include<stdio.h>
#include<windows.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include "vmath.h"
#include "Sphere.h"
#include<math.h> //for cos and sin
#define _USE_MATH_DEFINES 1

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "Sphere.lib")

using namespace vmath;

//Enum
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
int winWidth = 800;
int winHeight = 600;
bool gbDone = false;
bool gbFullscreen = false;
bool gbActive = false;
bool gbLight = false;
GLuint gShaderProgramObject_pv, gShaderProgramObject_pf;
GLuint vao_sphere;
GLuint vbo_sphere_position, vbo_sphere_element, vbo_sphere_normal;
GLuint mUniform, vUniform, pUniform;
GLuint laUniform_red, ldUniform_red, lsUniform_red, laUniform_blue, ldUniform_blue, lsUniform_blue, laUniform_green, ldUniform_green, lsUniform_green;
GLuint kaUniform, kdUniform, ksUniform;
GLuint materialShininessUniform, lightPositionUniform_red, lightPositionUniform_green, lightPositionUniform_blue, lIsPressed;
mat4 perspectiveProjectionMatrix;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices, gNumElements;
GLfloat angle_pyramid = 0.0f;
GLfloat lightAmbient_red[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse_red[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightSpecular_red[4] = { 1.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightPosition_red[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightAmbient_green[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightDiffuse_green[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightSpecular_green[4] = { 0.0f, 1.0f, 0.0f, 1.0f };
GLfloat lightPosition_green[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightAmbient_blue[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightDiffuse_blue[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightSpecular_blue[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
GLfloat lightPosition_blue[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess = 128.0f;
int keypress = 1;
GLfloat angleOfRotation = 0.0f;


//Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable Declaration
	int iRet;
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	//Function Declaration
	int initialize(void);
	void display(void);
	void update(void);

	//Open the log file and Error Checking
	if (fopen_s(&gbFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Created Successfully\n");
	}

	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;

	//Registering the window class
	RegisterClassEx(&wndclass);

	//Creating the Window in Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Call to Initialize function to Initialize OpenGL
	iRet = initialize();

	//Error checking for Initialization of  OpenGL
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Set Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Context Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization of OpenGL is successfull\n");
	}

	//Showing the Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				fprintf(gbFile, "Message = %d\n", msg.message);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void FullscreenToggle(void);

	switch (iMsg)
	{

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case VK_F1:
			FullscreenToggle();
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
			break;

		case 'V':
		case 'v':
			keypress = 1;
			break;

		case 'F':
		case 'f':
			keypress = 2;
			break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_SETFOCUS:
		gbActive = true;
		break;

	case WM_KILLFOCUS:
		gbActive = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle Function
void FullscreenToggle(void)
{
	//Variable Decalaration
	MONITORINFO mi;

	//Code
	if (gbFullscreen == false)
	{

		mi = { sizeof(MONITORINFO) };

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

//Initialize Function
int initialize(void)
{
	//Variable Declaration
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLuint VertexShaderObject_pv, VertexShaderObject_pf;
	GLuint FragmentShaderObject_pv, FragmentShaderObject_pf;

	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initializaton of PFD
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();

	if (result != GLEW_OK)
	{
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object for Per Vertex Lighting
	VertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lIsPressed;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_light_red;" \
		"out vec3 phong_ads_light_green;" \
		"out vec3 phong_ads_light_blue;" \
		"void main(void)" \
		"{" \
		"if(u_lIsPressed==1)" \
		"{" \
		"vec4 eye_coordinates = u_m_matrix * u_v_matrix * vPosition;" \
		"vec3 tNorm = normalize(mat3(u_m_matrix * u_v_matrix) * vNormal);" \
		"vec3 lightDirection_red = normalize(vec3(u_light_position_red - eye_coordinates));" \
		"vec3 lightDirection_green = normalize(vec3(u_light_position_green - eye_coordinates));" \
		"vec3 lightDirection_blue = normalize(vec3(u_light_position_blue - eye_coordinates));" \
		"float tn_dot_ld_red = max(dot(lightDirection_red, tNorm), 0.0);" \
		"vec3 reflectionVector_red = reflect(-lightDirection_red, tNorm);" \
		"float tn_dot_ld_green = max(dot(lightDirection_green, tNorm), 0.0);" \
		"vec3 reflectionVector_green = reflect(-lightDirection_green, tNorm);" \
		"float tn_dot_ld_blue = max(dot(lightDirection_blue, tNorm), 0.0);" \
		"vec3 reflectionVector_blue = reflect(-lightDirection_blue, tNorm);" \
		"vec3 viewerVector = normalize(vec3(-eye_coordinates));" \
		"vec3 ambient_red = u_la_red * u_ka;" \
		"vec3 diffuse_red = u_ld_red * u_kd * tn_dot_ld_red;" \
		"vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflectionVector_red, viewerVector), 0.0), u_material_shininess);" \
		"phong_ads_light_red = ambient_red + diffuse_red + specular_red;" \
		"vec3 ambient_green = u_la_green * u_ka;" \
		"vec3 diffuse_green = u_ld_green * u_kd * tn_dot_ld_green;" \
		"vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflectionVector_green, viewerVector), 0.0), u_material_shininess);" \
		"phong_ads_light_green = ambient_green + diffuse_green + specular_green;" \
		"vec3 ambient_blue = u_la_blue * u_ka; " \
		"vec3 diffuse_blue = u_ld_blue * u_kd * tn_dot_ld_blue; " \
		"vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflectionVector_blue, viewerVector), 0.0), u_material_shininess); " \
		"phong_ads_light_blue = ambient_blue + diffuse_blue + specular_blue; " \
		"}" \
		"else" \
		"{" \
		"phong_ads_light_red = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_light_green = vec3(1.0, 1.0, 1.0);" \
		"phong_ads_light_blue = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}";

	//Specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_pv, 1, (const GLchar **)&vertexShaderSourceCode_pv, NULL);

	//Compile the Vertex Shader
	glCompileShader(VertexShaderObject_pv);

	//Error Checking code
	glGetShaderiv(VertexShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_pv, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Vertex Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object for Per Vertex Lighting
	FragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light_red;" \
		"in vec3 phong_ads_light_green;" \
		"in vec3 phong_ads_light_blue;" \
		"uniform int u_lIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lIsPressed==1)" \
		"{" \
		"FragColor = vec4(phong_ads_light_red + phong_ads_light_green + phong_ads_light_blue, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"}";

	//Specify above source code to the fragment shader object
	glShaderSource(FragmentShaderObject_pv, 1, (const GLchar **)&fragmentShaderSourceCode_pv, NULL);

	//Compile the Fragment Shader
	glCompileShader(FragmentShaderObject_pv);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	//Error Checking code
	glGetShaderiv(FragmentShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_pv, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Fragment Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program for Per Vertex Lighting
	gShaderProgramObject_pv = glCreateProgram();

	//Attach Vertex Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pv, VertexShaderObject_pv);

	//Attach Fragment Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pv, FragmentShaderObject_pv);

	//Pre-Linking Binding to Vertex Attributes
	glBindAttribLocation(gShaderProgramObject_pv, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pv, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link the Shader Program for Per Vertex Lighting
	glLinkProgram(gShaderProgramObject_pv);

	//Error checking code
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_pv, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Shader Program Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retrieving of Uniform Locations for Per Vertex Lighting
	mUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_p_matrix");
	laUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_la_red");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_red");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_red");
	laUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_la_green");
	ldUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_green");
	lsUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_green");
	laUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_la_blue");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_blue");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_blue");
	kaUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ks");
	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position_red");
	lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position_green");
	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position_blue");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_material_shininess");
	lIsPressed = glGetUniformLocation(gShaderProgramObject_pv, "u_lIsPressed");

	//Define Vertex Shader Object for Per Fragment Lighting
	VertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lIsPressed;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec4 u_light_position_blue;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirection_red;" \
		"out vec3 lightDirection_green;" \
		"out vec3 lightDirection_blue;" \
		"out vec3 viewerVector;" \
		"void main(void)" \
		"{" \
		"vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;" \
		"tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;" \
		"lightDirection_red = vec3(u_light_position_red - eyeCoordinates);" \
		"lightDirection_green = vec3(u_light_position_green - eyeCoordinates);" \
		"lightDirection_blue = vec3(u_light_position_blue - eyeCoordinates);" \
		"viewerVector = vec3(-eyeCoordinates);" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}";

	//Specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_pf, 1, (const GLchar **)&vertexShaderSourceCode_pf, NULL);

	//Compile the Vertex Shader
	glCompileShader(VertexShaderObject_pf);

	//Error Checking code
	glGetShaderiv(VertexShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_pf, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Vertex Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object for Per Fragment Lighting
	FragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"in vec3 tNorm;" \
		"in vec3 lightDirection_red;" \
		"in vec3 lightDirection_green;" \
		"in vec3 lightDirection_blue;" \
		"in vec3 viewerVector;" \
		"uniform int u_lIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lIsPressed==1)" \
		"{" \
		"vec3 normalizedTNorm = normalize(tNorm);" \
		"vec3 normalizedLightDirection_red = normalize(lightDirection_red);" \
		"vec3 normalizedLightDirection_green = normalize(lightDirection_green);" \
		"vec3 normalizedLightDirection_blue = normalize(lightDirection_blue);" \
		"vec3 normalizedViewerVector = normalize(viewerVector);" \
		"float tn_dot_ld_red = max(dot(normalizedLightDirection_red, normalizedTNorm), 0.0);" \
		"float tn_dot_ld_green = max(dot(normalizedLightDirection_green, normalizedTNorm), 0.0);" \
		"float tn_dot_ld_blue = max(dot(normalizedLightDirection_blue, normalizedTNorm), 0.0);" \
		"vec3 reflectionVector_red = reflect(-normalizedLightDirection_red, normalizedTNorm);" \
		"vec3 reflectionVector_green = reflect(-normalizedLightDirection_green, normalizedTNorm);" \
		"vec3 reflectionVector_blue = reflect(-normalizedLightDirection_blue, normalizedTNorm);" \
		"vec3 ambient_red = u_la_red * u_ka;" \
		"vec3 diffuse_red = u_ld_red * u_kd * tn_dot_ld_red;" \
		"vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflectionVector_red, normalizedViewerVector), 0.0), u_material_shininess);" \
		"vec3 phong_ads_light_red = ambient_red + diffuse_red + specular_red;" \
		"vec3 ambient_green = u_la_green * u_ka;" \
		"vec3 diffuse_green = u_ld_green * u_kd * tn_dot_ld_green;" \
		"vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflectionVector_green, normalizedViewerVector), 0.0), u_material_shininess);" \
		"vec3 phong_ads_light_green = ambient_green + diffuse_green + specular_green;" \
		"vec3 ambient_blue = u_la_blue * u_ka; " \
		"vec3 diffuse_blue = u_ld_blue * u_kd * tn_dot_ld_blue; " \
		"vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflectionVector_blue, normalizedViewerVector), 0.0), u_material_shininess); " \
		"vec3 phong_ads_light_blue = ambient_blue + diffuse_blue + specular_blue; " \
		"FragColor = vec4(phong_ads_light_red + phong_ads_light_green + phong_ads_light_blue, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"}";

	//Specify above source code to the fragment shader object
	glShaderSource(FragmentShaderObject_pf, 1, (const GLchar **)&fragmentShaderSourceCode_pf, NULL);

	//Compile the Fragment Shader
	glCompileShader(FragmentShaderObject_pf);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	//Error Checking code
	glGetShaderiv(FragmentShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_pf, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Fragment Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program for Per Fragment Lighting
	gShaderProgramObject_pf = glCreateProgram();

	//Attach Vertex Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pf, VertexShaderObject_pf);

	//Attach Fragment Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pf, FragmentShaderObject_pf);

	//Pre-Linking Binding to Vertex Attributes
	glBindAttribLocation(gShaderProgramObject_pf, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pf, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_pf);

	//Error checking code
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_pf, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Shader Program Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retrieving of Uniform Locations for Per Fragment Lighting
	mUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_p_matrix");
	laUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_la_red");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_red");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_red");
	laUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_la_green");
	ldUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_green");
	lsUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_green");
	laUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_la_blue");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_blue");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_blue");
	kaUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ks");
	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position_red");
	lightPositionUniform_green = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position_green");
	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position_blue");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_material_shininess");
	lIsPressed = glGetUniformLocation(gShaderProgramObject_pv, "u_lIsPressed");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//Create vao and vbo for Sphere
	// vao
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	// position vbo
	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &vbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	//Clear the Screen with OpenGL Color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Warmup call for resize
	resize(winWidth, winHeight);

	return 0;
}

//Display Function
void display(void)
{
	//Declaration of Matrices
	mat4 translateMatrix;
	mat4 rotationMatrix;
	mat4 modelMatrix;
	mat4 viewMatrix;
	//mat4 projectionMatrix;

	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (keypress == 1)
	{
		glUseProgram(gShaderProgramObject_pv);
	}
	else if (keypress == 2)
	{
		glUseProgram(gShaderProgramObject_pf);
	}

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	//projectionMatrix = mat4::identity();

	//Do necessary transformation for Cube
	translateMatrix = translate(0.0f, 0.0f, -3.0f);
	modelMatrix = modelMatrix * translateMatrix;

	//Do necessary matrix multiplication
	//projectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == true)
	{
		lightPosition_red[0] =  4 * cos(angleOfRotation);
		lightPosition_red[1] = 4 * sin(angleOfRotation);
		
		lightPosition_green[0] = 4 * cos(angleOfRotation);
		lightPosition_green[2] = 4 * sin(angleOfRotation);

		lightPosition_blue[1] = 4 * cos(angleOfRotation);
		lightPosition_blue[2] = 4 * sin(angleOfRotation);

		glUniform1i(lIsPressed, 1);
		glUniform3fv(laUniform_red, 1, lightAmbient_red);
		glUniform3fv(ldUniform_red, 1, lightDiffuse_red);
		glUniform3fv(lsUniform_red, 1, lightSpecular_red);
		glUniform3fv(laUniform_green, 1, lightAmbient_green);
		glUniform3fv(ldUniform_green, 1, lightDiffuse_green);
		glUniform3fv(lsUniform_green, 1, lightSpecular_green);
		glUniform3fv(laUniform_blue, 1, lightAmbient_blue);
		glUniform3fv(ldUniform_blue, 1, lightDiffuse_blue);
		glUniform3fv(lsUniform_blue, 1, lightSpecular_blue);
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShininessUniform, materialShininess);
		glUniform4fv(lightPositionUniform_red, 1, lightPosition_red);
		glUniform4fv(lightPositionUniform_green, 1, lightPosition_green);
		glUniform4fv(lightPositionUniform_blue, 1, lightPosition_blue);
	}
	else
	{
		glUniform1i(lIsPressed, 0);
	}

	//Bind with vao for Sphere
	glBindVertexArray(vao_sphere);

	//Draw the Sphere
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//Unbin with vbo
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Unbind with vao for Sphere
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

//Resize Function
void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.f);
}

//Update Function
void update(void)
{
	angleOfRotation = angleOfRotation + 0.1f;

	if (angleOfRotation >= 360)
	{
		angleOfRotation = 0.0f;
	}

}

//Unintialize Function
void uninitialize(void)
{
	//Code
	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_element)
	{
		glDeleteBuffers(1, &vbo_sphere_element);
		vbo_sphere_element = 0;
	}

	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	if (gShaderProgramObject_pv)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_pv);

		//Get Shader count from the program
		glGetProgramiv(gShaderProgramObject_pv, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_pv, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Detach Shaders one by one
				glDetachShader(gShaderProgramObject_pv, pShaders[shaderNumber]);

				//Delete detached Shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_pv);
		gShaderProgramObject_pv = 0;
		glUseProgram(0);
	}

	if (gShaderProgramObject_pf)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_pf);

		//Get Shader count from the program
		glGetProgramiv(gShaderProgramObject_pf, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_pf, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Detach Shaders one by one
				glDetachShader(gShaderProgramObject_pf, pShaders[shaderNumber]);

				//Delete detached Shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_pf);
		gShaderProgramObject_pf = 0;
		glUseProgram(0);
	}

	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log file is Closed Successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}