#include<stdio.h>
#include<windows.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include "vmath.h"
#include "Sphere.h"
#include<math.h> //for cos and sin
#define _USE_MATH_DEFINES 1

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment (lib, "Sphere.lib")

using namespace vmath;

//Enum
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
int winWidth = 800;
int winHeight = 600;
bool gbDone = false;
bool gbFullscreen = false;
bool gbActive = false;
bool gbLight = false;
GLuint gShaderProgramObject_pv, gShaderProgramObject_pf;
GLuint vao_sphere;
GLuint vbo_sphere_position, vbo_sphere_element, vbo_sphere_normal;
GLuint mUniform, vUniform, pUniform;
GLuint laUniform, ldUniform, lsUniform, kaUniform, kdUniform, ksUniform;
GLuint materialShininessUniform, lightPositionUniform, lIsPressed;
mat4 perspectiveProjectionMatrix;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumVertices, gNumElements;
GLfloat lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
int keypress = 1;
int axiskey = 0;
GLfloat angleOfRotation = 0.0f;
mat4 translateMatrix;
mat4 rotationMatrix;
mat4 modelMatrix;
mat4 viewMatrix;

//Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable Declaration
	int iRet;
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	//Function Declaration
	int initialize(void);
	void display(void);
	void update(void);

	//Open the log file and Error Checking
	if (fopen_s(&gbFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Created Successfully\n");
	}

	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;

	//Registering the window class
	RegisterClassEx(&wndclass);

	//Creating the Window in Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Call to Initialize function to Initialize OpenGL
	iRet = initialize();

	//Error checking for Initialization of  OpenGL
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Set Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Context Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization of OpenGL is successfull\n");
	}

	//Showing the Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				fprintf(gbFile, "Message = %d\n", msg.message);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void FullscreenToggle(void);

	switch (iMsg)
	{

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case VK_F1:
			FullscreenToggle();
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLight == false)
			{
				gbLight = true;
			}
			else
			{
				gbLight = false;
			}
			break;

		case 'V':
		case 'v':
			keypress = 1;
			break;

		case 'F':
		case 'f':
			keypress = 2;
			break;

		case 'X':
		case 'x':
			axiskey = 1;
			break;

		case 'Y':
		case 'y':
			axiskey = 2;
			break;

		case 'Z':
		case 'z':
			axiskey = 3;
			break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_SETFOCUS:
		gbActive = true;
		break;

	case WM_KILLFOCUS:
		gbActive = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle Function
void FullscreenToggle(void)
{
	//Variable Decalaration
	MONITORINFO mi;

	//Code
	if (gbFullscreen == false)
	{

		mi = { sizeof(MONITORINFO) };

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

//Initialize Function
int initialize(void)
{
	//Variable Declaration
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLuint VertexShaderObject_pv, VertexShaderObject_pf;
	GLuint FragmentShaderObject_pv, FragmentShaderObject_pf;

	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initializaton of PFD
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();

	if (result != GLEW_OK)
	{
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object for Per Vertex Lighting
	VertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lIsPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform vec4 u_light_position;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lIsPressed==1)" \
		"{" \
		"vec4 eye_coordinates = u_v_matrix * u_m_matrix * vPosition;" \
		"vec3 tNorm = normalize(mat3(u_m_matrix * u_v_matrix) * vNormal);" \
		"vec3 lightDirection = normalize(vec3(u_light_position - eye_coordinates));" \
		"float tn_dot_ld = max(dot(lightDirection, tNorm), 0.0);" \
		"vec3 reflectionVector = reflect(-lightDirection, tNorm);" \
		"vec3 viewerVector = normalize(vec3(-eye_coordinates));" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, viewerVector), 0.0), u_material_shininess);" \
		"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}";

	//Specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_pv, 1, (const GLchar **)&vertexShaderSourceCode_pv, NULL);

	//Compile the Vertex Shader
	glCompileShader(VertexShaderObject_pv);

	//Error Checking code
	glGetShaderiv(VertexShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_pv, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Vertex Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object for Per Vertex Lighting
	FragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"uniform int u_lIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lIsPressed==1)" \
		"{" \
		"FragColor = vec4(phong_ads_light, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"}";

	//Specify above source code to the fragment shader object
	glShaderSource(FragmentShaderObject_pv, 1, (const GLchar **)&fragmentShaderSourceCode_pv, NULL);

	//Compile the Fragment Shader
	glCompileShader(FragmentShaderObject_pv);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	//Error Checking code
	glGetShaderiv(FragmentShaderObject_pv, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_pv, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Fragment Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program for Per Vertex Lighting
	gShaderProgramObject_pv = glCreateProgram();

	//Attach Vertex Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pv, VertexShaderObject_pv);

	//Attach Fragment Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pv, FragmentShaderObject_pv);

	//Pre-Linking Binding to Vertex Attributes
	glBindAttribLocation(gShaderProgramObject_pv, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pv, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link the Shader Program for Per Vertex Lighting
	glLinkProgram(gShaderProgramObject_pv);

	//Error checking code
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_pv, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Shader Program Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retrieving of Uniform Locations for Per Vertex Lighting
	mUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ls");
	kaUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_ks");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_material_shininess");
	lIsPressed = glGetUniformLocation(gShaderProgramObject_pv, "u_lIsPressed");

	//Define Vertex Shader Object for Per Fragment Lighting
	VertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lIsPressed;" \
		"uniform vec4 u_light_position;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirection;" \
		"out vec3 viewerVector;" \
		"void main(void)" \
		"{" \
		"vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;" \
		"tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;" \
		"lightDirection = vec3(u_light_position - eyeCoordinates);" \
		"viewerVector = vec3(-eyeCoordinates);" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}";

	//Specify above source code to the vertex shader object
	glShaderSource(VertexShaderObject_pf, 1, (const GLchar **)&vertexShaderSourceCode_pf, NULL);

	//Compile the Vertex Shader
	glCompileShader(VertexShaderObject_pf);

	//Error Checking code
	glGetShaderiv(VertexShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(VertexShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(VertexShaderObject_pf, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Vertex Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object for Per Fragment Lighting
	FragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"in vec3 tNorm;" \
		"in vec3 lightDirection;" \
		"in vec3 viewerVector;" \
		"uniform int u_lIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lIsPressed==1)" \
		"{" \
		"vec3 normalizedTNorm = normalize(tNorm);" \
		"vec3 normalizedLightDirection = normalize(lightDirection);" \
		"vec3 normalizedViewerVector = normalize(viewerVector);" \
		"float tn_dot_ld = max(dot(normalizedLightDirection, normalizedTNorm), 0.0);" \
		"vec3 reflectionVector = reflect(-normalizedLightDirection, normalizedTNorm);" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalizedViewerVector), 0.0), u_material_shininess);" \
		"vec3 phong_ads_light = ambient + diffuse + specular;" \
		"FragColor = vec4(phong_ads_light, 1.0);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"}" \
		"}";

	//Specify above source code to the fragment shader object
	glShaderSource(FragmentShaderObject_pf, 1, (const GLchar **)&fragmentShaderSourceCode_pf, NULL);

	//Compile the Fragment Shader
	glCompileShader(FragmentShaderObject_pf);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	//Error Checking code
	glGetShaderiv(FragmentShaderObject_pf, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(FragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(FragmentShaderObject_pf, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Fragment Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program for Per Fragment Lighting
	gShaderProgramObject_pf = glCreateProgram();

	//Attach Vertex Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pf, VertexShaderObject_pf);

	//Attach Fragment Shader to the Shader Program
	glAttachShader(gShaderProgramObject_pf, FragmentShaderObject_pf);

	//Pre-Linking Binding to Vertex Attributes
	glBindAttribLocation(gShaderProgramObject_pf, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject_pf, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject_pf);

	//Error checking code
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_pf, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Shader Program Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retrieving of Uniform Locations for Per Fragment Lighting
	mUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ls");
	kaUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ks");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_light_position");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_material_shininess");
	lIsPressed = glGetUniformLocation(gShaderProgramObject_pf, "u_lIsPressed");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//Create vao and vbo for Sphere
	// vao
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	// position vbo
	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &vbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &vbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	//Clear the Screen with OpenGL Color
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	//Warmup call for resize
	resize(winWidth, winHeight);

	return 0;
}

//Display Function
void display(void)
{
	//Function Declaration
	void draw24Spheres(void);

	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (keypress == 1)
	{
		glUseProgram(gShaderProgramObject_pv);
	}
	else if (keypress == 2)
	{
		glUseProgram(gShaderProgramObject_pf);
	}

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

	if (gbLight == true)
	{
		glUniform1i(lIsPressed, 1);
		glUniform3fv(laUniform, 1, lightAmbient);
		glUniform3fv(ldUniform, 1, lightDiffuse);
		glUniform3fv(lsUniform, 1, lightSpecular);
		glUniform4fv(lightPositionUniform, 1, lightPosition);
	}
	else
	{
		glUniform1i(lIsPressed, 0);
	}

	if (axiskey == 1)
	{
		lightPosition[0] = 0;
		lightPosition[1] = 6 * cos(angleOfRotation);
		lightPosition[2] = -3 + (6 * sin(angleOfRotation));
	}
	else if (axiskey == 2)
	{
		lightPosition[0] = 6 * cos(angleOfRotation);
		lightPosition[1] = 0;
		lightPosition[2] = -3 + (6 * sin(angleOfRotation));
	}
	else if (axiskey == 3)
	{
		lightPosition[0] = 6 * cos(angleOfRotation);
		lightPosition[1] = 6 * sin(angleOfRotation);
		lightPosition[2] = 0;
	}
	
	//Bind with vao for Sphere
	glBindVertexArray(vao_sphere);

	//Draw the Sphere
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	draw24Spheres();

	//Unbin with vbo
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Unbind with vao for Sphere
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

//Resize Function
void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}

	//glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.f);
}

//Update Function
void update(void)
{
	angleOfRotation = angleOfRotation + 0.1f;

	if (angleOfRotation >= 360.0f)
	{
		angleOfRotation = 0.0f;
	}
}

void draw24Spheres(void)
{
	GLfloat materialAmbient[4], materialDiffuse[4], materialSpecular[4], materialShininess;

	//1st column
	materialAmbient[0] = 0.0215f;
	materialAmbient[1] = 0.1745f;
	materialAmbient[2] = 0.0215f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.07568f;
	materialDiffuse[1] = 0.61424f;
	materialDiffuse[2] = 0.07568f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.633f;
	materialSpecular[1] = 0.727811f;
	materialSpecular[2] = 0.633f;
	materialSpecular[3] = 1.0f;

	materialShininess = 0.6f * 128.0f;

	glViewport(0, (GLsizei)winHeight / 1.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	modelMatrix = mat4::identity();
	translateMatrix = translate(0.0f, 0.0f, -3.0f);
	modelMatrix = modelMatrix * translateMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);
	
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.135f;
	materialAmbient[1] = 0.2225f;
	materialAmbient[2] = 0.1575f;
	materialAmbient[3] = 1.0f;

	materialDiffuse[0] = 0.54f;
	materialDiffuse[1] = 0.89f;
	materialDiffuse[2] = 0.63f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.316228f;
	materialSpecular[1] = 0.316228f;
	materialSpecular[2] = 0.316228f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.1f * 128.0f;
	
	glViewport(0, (GLsizei)winHeight / 1.65, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.05375f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.06625f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.18275f;
	materialDiffuse[1] = 0.17f;
	materialDiffuse[2] = 0.22525f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.332741f;
	materialSpecular[1] = 0.328634f;
	materialSpecular[2] = 0.346435f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.3f * 128.0f;
	
	glViewport(0, (GLsizei)winHeight / 2.2, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.20725f;
	materialAmbient[2] = 0.20725f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 1.0f;
	materialDiffuse[1] = 0.829f;
	materialDiffuse[2] = 0.829f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.296648f;
	materialSpecular[1] = 0.296648f;
	materialSpecular[2] = 0.296648f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.088f * 128.0f;
	
	glViewport(0, (GLsizei)winHeight / 3.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.1745f;
	materialAmbient[1] = 0.01175f;
	materialAmbient[2] = 0.01175f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.61424f;
	materialDiffuse[1] = 0.04136f;
	materialDiffuse[2] = 0.04136f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.727811f;
	materialSpecular[1] = 0.626959f;
	materialSpecular[2] = 0.626959f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.6f * 128.0f;

	glViewport(0, (GLsizei)winHeight / 6.5, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.1f;
	materialAmbient[1] = 0.18725f;
	materialAmbient[2] = 0.1745f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.396f;
	materialDiffuse[1] = 0.74151f;
	materialDiffuse[2] = 0.69102f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.297254f;
	materialSpecular[1] = 0.30829f;
	materialSpecular[2] = 0.306678f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.1f * 128.0f;
	
	glViewport(0, 0, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//2nd column
	materialAmbient[0] = 0.329412f;
	materialAmbient[1] = 0.223529f;
	materialAmbient[2] = 0.027451f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.780392f;
	materialDiffuse[1] = 0.568627f;
	materialDiffuse[2] = 0.113725f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.992157f;
	materialSpecular[1] = 0.941176f;
	materialSpecular[2] = 0.807843f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.21794872f * 128.0f;
	
	materialShininess = 0.6f * 128.0f;

	glViewport((GLsizei)winWidth / 4.5, (GLsizei)winHeight / 1.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.2125f;
	materialAmbient[1] = 0.1275f;
	materialAmbient[2] = 0.054f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.714f;
	materialDiffuse[1] = 0.4284f;
	materialDiffuse[2] = 0.18144f;
	materialDiffuse[3] = 1.0f;

	materialSpecular[0] = 0.393548f;
	materialSpecular[1] = 0.271906f;
	materialSpecular[2] = 0.166721f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.2f * 128.0f;
	
	glViewport((GLsizei)winWidth / 4.5, (GLsizei)winHeight / 1.65, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.25f;
	materialAmbient[1] = 0.25f;
	materialAmbient[2] = 0.25f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.774597f;
	materialSpecular[1] = 0.774597f;
	materialSpecular[2] = 0.774597f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.6f * 128.0f;
	
	glViewport((GLsizei)winWidth / 4.5, (GLsizei)winHeight / 2.2, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.19125f;
	materialAmbient[1] = 0.0735f;
	materialAmbient[2] = 0.0225f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.7038f;
	materialDiffuse[1] = 0.27048f;
	materialDiffuse[2] = 0.0828f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.256777f;
	materialSpecular[1] = 0.137622f;
	materialSpecular[2] = 0.086014f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.1f * 128.0f;
	
	glViewport((GLsizei)winWidth / 4.5, (GLsizei)winHeight / 3.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.24725f;
	materialAmbient[1] = 0.1995f;
	materialAmbient[2] = 0.0745f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.75164f;
	materialDiffuse[1] = 0.60648f;
	materialDiffuse[2] = 0.22648f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.628281f;
	materialSpecular[1] = 0.555802f;
	materialSpecular[2] = 0.366065f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.4f * 128.0f;
	
	glViewport((GLsizei)winWidth / 4.5, (GLsizei)winHeight / 6.5, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.19225f;
	materialAmbient[1] = 0.19225f;
	materialAmbient[2] = 0.19225f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.50754f;
	materialDiffuse[1] = 0.50754f;
	materialDiffuse[2] = 0.50754f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.508273f;
	materialSpecular[1] = 0.508273f;
	materialSpecular[2] = 0.508273f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.4f * 128.0f;
	
	glViewport((GLsizei)winWidth / 4.5, 0, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//3rd column
	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.01;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.50;
	materialSpecular[1] = 0.50f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.25f * 128.0f;
	
	glViewport((GLsizei)winWidth / 2.2, (GLsizei)winHeight / 1.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.1f;
	materialAmbient[2] = 0.06f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.0f;
	materialDiffuse[1] = 0.50980392f;
	materialDiffuse[2] = 0.50980392f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.50196078f;
	materialSpecular[1] = 0.50196078f;
	materialSpecular[2] = 0.50196078f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.25f * 128.0f;
	
	glViewport((GLsizei)winWidth / 2.2, (GLsizei)winHeight / 1.65, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.1f;
	materialDiffuse[1] = 0.35f;
	materialDiffuse[2] = 0.1f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.45f;
	materialSpecular[1] = 0.55f;
	materialSpecular[2] = 0.45f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.25f * 128.0f;
	
	glViewport((GLsizei)winWidth / 2.2, (GLsizei)winHeight / 2.2, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.0f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.6f;
	materialSpecular[2] = 0.6f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.25f * 128.0f;
	
	glViewport((GLsizei)winWidth / 2.2, (GLsizei)winHeight / 3.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.55f;
	materialDiffuse[1] = 0.55f;
	materialDiffuse[2] = 0.55f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.70f;
	materialSpecular[1] = 0.70f;
	materialSpecular[2] = 0.70f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.25f * 128.0f;
	
	glViewport((GLsizei)winWidth / 2.2, (GLsizei)winHeight / 6.5, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.60f;
	materialSpecular[1] = 0.60f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.25f * 128.0f;
	
	glViewport((GLsizei)winWidth / 2.2, 0, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//4th column
	materialAmbient[0] = 0.02f;
	materialAmbient[1] = 0.02f;
	materialAmbient[2] = 0.02f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.01;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.4f;
	materialSpecular[1] = 0.4f;
	materialSpecular[2] = 0.4f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.078125f * 128.0f;
	
	glViewport((GLsizei)winWidth / 1.45, (GLsizei)winHeight / 1.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.04f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.078125f * 128.0f;
	
	glViewport((GLsizei)winWidth / 1.45, (GLsizei)winHeight / 1.65, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.0f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.04f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.078125f * 128.0f;
	
	glViewport((GLsizei)winWidth / 1.45, (GLsizei)winHeight / 2.2, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.0f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.04f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.078125f * 128.0f;
	
	glViewport((GLsizei)winWidth / 1.45, (GLsizei)winHeight / 3.3, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.05f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.078125f * 128.0f;
	
	glViewport((GLsizei)winWidth / 1.45, (GLsizei)winHeight / 6.5, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	materialAmbient[0] = 0.05f;
	materialAmbient[1] = 0.05f;
	materialAmbient[2] = 0.0f;
	materialAmbient[3] = 1.0f;
	
	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	
	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	
	materialShininess = 0.078125f * 128.0f;
	
	glViewport((GLsizei)winWidth / 1.45, 0, (GLsizei)winWidth / 4, (GLsizei)winHeight / 4);

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShininessUniform, materialShininess);

	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
}

//Unintialize Function
void uninitialize(void)
{
	//Code
	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);
		vbo_sphere_position = 0;
	}

	if (vbo_sphere_normal)
	{
		glDeleteBuffers(1, &vbo_sphere_normal);
		vbo_sphere_normal = 0;
	}

	if (vbo_sphere_element)
	{
		glDeleteBuffers(1, &vbo_sphere_element);
		vbo_sphere_element = 0;
	}

	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	if (gShaderProgramObject_pv)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_pv);

		//Get Shader count from the program
		glGetProgramiv(gShaderProgramObject_pv, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_pv, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Detach Shaders one by one
				glDetachShader(gShaderProgramObject_pv, pShaders[shaderNumber]);

				//Delete detached Shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_pv);
		gShaderProgramObject_pv = 0;
		glUseProgram(0);
	}

	if (gShaderProgramObject_pf)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject_pf);

		//Get Shader count from the program
		glGetProgramiv(gShaderProgramObject_pf, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_pf, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Detach Shaders one by one
				glDetachShader(gShaderProgramObject_pf, pShaders[shaderNumber]);

				//Delete detached Shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_pf);
		gShaderProgramObject_pf = 0;
		glUseProgram(0);
	}

	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log file is Closed Successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}