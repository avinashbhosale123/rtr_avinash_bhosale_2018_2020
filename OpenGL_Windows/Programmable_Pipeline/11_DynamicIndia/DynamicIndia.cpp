#include<stdio.h>
#include<windows.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include<Mmsystem.h>
#include "vmath.h"

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glew32.lib")
#pragma comment(lib, "Winmm.lib")

using namespace vmath;

//Enum
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
int winWidth = 800;
int winHeight = 600;
bool gbDone = false;
bool gbFullscreen = false;
bool gbActive = false;
GLuint gShaderProgramObject;
GLuint vao_il, vao_n, vao_d, vao_ir, vao_a, vao_strips, vao_plane, vao_exhaust;
GLuint vbo_il_position, vbo_il_color, vbo_n_position, vbo_n_color, vbo_d_position, vbo_d_color, vbo_plane_position, vbo_plane_color;
GLuint vbo_ir_position, vbo_ir_color, vbo_a_position, vbo_a_color, vbo_strips_position, vbo_strips_color, vbo_exhaust_position, vbo_exhaust_color;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

//Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable Declaration
	int iRet;
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	//Function Declaration
	int initialize(void);
	void display(void);
	void update(void);

	//Open the log file and Error Checking
	if (fopen_s(&gbFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Created Successfully\n");
	}

	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;

	//Registering the window class
	RegisterClassEx(&wndclass);

	//Creating the Window in Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Call to Initialize function to Initialize OpenGL
	iRet = initialize();

	//Error checking for Initialization of  OpenGL
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Set Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Context Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization of OpenGL is successfull\n");
	}

	//Showing the Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	PlaySound(MAKEINTRESOURCE(101), NULL, SND_RESOURCE | SND_ASYNC | SND_NODEFAULT);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				fprintf(gbFile, "Message = %d\n", msg.message);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void FullscreenToggle(void);

	switch (iMsg)
	{

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		//case 0x46:
			//FullscreenToggle();
			//break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_SETFOCUS:
		gbActive = true;
		break;

	case WM_KILLFOCUS:
		gbActive = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle Function
void FullscreenToggle(void)
{
	//Variable Decalaration
	MONITORINFO mi;

	//Code
	if (gbFullscreen == false)
	{

		mi = { sizeof(MONITORINFO) };

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
}

//Initialize Function
int initialize(void)
{
	//Variable Declaration
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initializaton of PFD
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();

	if (result != GLEW_OK)
	{
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	//Specify above source code to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	//Error Checking code
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Vertex Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";

	//Specify above source code to the fragment shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//Compile the Fragment Shader
	glCompileShader(gFragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	//Error Checking code
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Fragment Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach Fragment Shader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Pre-Linking Binding to Vertex Attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error checking code
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Shader Program Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retrieving of Uniform Locations
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	GLfloat ilVertices[] = { -0.60f, 0.60f, 0.0f, -0.50f, 0.60f, 0.0f,
							-0.55f, 0.60f, 0.0f, -0.55f, -0.60f, 0.0f,
							-0.60f, -0.60f, 0.0f, -0.50f, -0.60f, 0.0f };

	GLfloat ilColor[] = { 1.00f, 0.60f, 0.20f, 1.00f, 0.60f, 0.20f,
						1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f,
						0.07f, 0.53f, 0.027f, 0.07f, 0.53f, 0.027f };

	GLfloat nVertices[] = { -0.40f, 0.60f, 0.00f, -0.40f, -0.60f, 0.0f,
							-0.40f, 0.60f, 0.00f, -0.20f, -0.60f, 0.0f,
							-0.20f, -0.60f, 0.0f, -0.20f, 0.60f, 0.0f };

	GLfloat nColor[] = { 1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f,
						1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f,
						0.07f, 0.53f, 0.027f, 1.00f, 0.60f, 0.20f };

	GLfloat dVertices[] = { -0.10f, 0.60f, 0.00f, -0.10f, -0.60f, 0.0f,
							-0.15f, 0.60f, 0.00f, 0.10f, 0.60f, 0.00f,
							0.10f, 0.60f, 0.00f, 0.10f, -0.60f, 0.0f,
							0.10f, -0.60f, 0.0f, -0.15f, -0.60f, 0.00f };

	GLfloat dColor[] = { 1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f,
						1.00f, 0.60f, 0.20f, 1.00f, 0.60f, 0.20f,
						1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f,
						0.07f, 0.53f, 0.027f, 0.07f, 0.53f, 0.027f };

	GLfloat irVertices[] = { 0.20f, 0.60f, 0.0f, 0.30f, 0.60f, 0.0f,
							0.25f, 0.60f, 0.0f, 0.25f, -0.60f, 0.0f,
							0.20f, -0.60f, 0.0f, 0.30f, -0.60f, 0.0f };

	GLfloat irColor[] = { 1.00f, 0.60f, 0.20f, 1.00f, 0.60f, 0.20f,
						1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f,
						0.07f, 0.53f, 0.027f, 0.07f, 0.53f, 0.027f };

	GLfloat aVertices[] = { 0.60f, -0.60f, 0.0f, 0.50f, 0.60f, 0.0f,
							0.50f, 0.60f, 0.0f, 0.40f, -0.60f, 0.0f };

	GLfloat aColor[] = { 0.07f, 0.53f, 0.027f, 1.00f, 0.60f, 0.20f,
						1.00f, 0.60f, 0.20f, 0.07f, 0.53f, 0.027f };

	GLfloat stripsVertices[] = { -0.048f, 0.02f, 0.0f, 0.048f, 0.02f, 0.0f,
								-0.05f, 0.0f, 0.0f, 0.05f, 0.0f, 0.0f,
								-0.052f, -0.02f, 0.0f, 0.052f, -0.02f, 0.0f };

	GLfloat stripsColor[] = { 1.00f, 0.60f, 0.20f, 1.00f, 0.60f, 0.20f,
							1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
							0.07f, 0.53f, 0.027f, 0.07f, 0.53f, 0.027f };

	GLfloat planeVertices[] = { 0.0f, 0.025f, 0.0f, -0.05f, 0.0f, 0.0f, 0.05f, 0.0f, 0.0f, 
								0.0f, -0.025f, 0.0f, 0.05f, 0.0f, 0.0f, -0.05f, 0.0f, 0.0f,
								-0.05f, 0.0f, 0.0f, -0.075f, 0.025f, 0.0f, -0.075f, -0.025f, 0.0f, 
								0.00f, -0.05f, 0.0f, -0.025f, 0.0f, 0.0f, 0.025f, 0.0f, 0.0f,
								0.0f, 0.05f, 0.0f, -0.025f, 0.0f, 0.0f, 0.025f, 0.0f, 0.0f };

	GLfloat planeColor[] = { 0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f, 
							0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f,
							0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f,
							0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f,
							0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f, 0.726f, 0.882f, 0.929f };

	GLfloat exhaustColor[] = { 0.0f, 0.0f, 0.0f, 1.00f, 0.60f, 0.20f,
								0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 
								0.0f, 0.0f, 0.0f, 0.07f, 0.53f, 0.027f };

	//Create vao and vbo for Left I
	glGenVertexArrays(1, &vao_il);
	glBindVertexArray(vao_il);

	glGenBuffers(1, &vbo_il_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_il_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ilVertices), ilVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_il_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_il_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ilColor), ilColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for N
	glGenVertexArrays(1, &vao_n);
	glBindVertexArray(vao_n);

	glGenBuffers(1, &vbo_n_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_n_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_n_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_n_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for D
	glGenVertexArrays(1, &vao_d);
	glBindVertexArray(vao_d);

	glGenBuffers(1, &vbo_d_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_d_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_d_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_d_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(24 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for Right I
	glGenVertexArrays(1, &vao_ir);
	glBindVertexArray(vao_ir);

	glGenBuffers(1, &vbo_ir_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_ir_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(irVertices), irVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_ir_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_ir_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(irColor), irColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for A
	glGenVertexArrays(1, &vao_a);
	glBindVertexArray(vao_a);

	glGenBuffers(1, &vbo_a_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_a_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_a_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_a_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for Strips
	glGenVertexArrays(1, &vao_strips);
	glBindVertexArray(vao_strips);

	glGenBuffers(1, &vbo_strips_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_strips_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(stripsVertices), stripsVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_strips_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_strips_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(stripsColor), stripsColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for Strips
	glGenVertexArrays(1, &vao_plane);
	glBindVertexArray(vao_plane);

	glGenBuffers(1, &vbo_plane_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_plane_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_plane_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_plane_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeColor), planeColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for Strips
	glGenVertexArrays(1, &vao_exhaust);
	glBindVertexArray(vao_exhaust);

	glGenBuffers(1, &vbo_exhaust_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_exhaust_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(18 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_exhaust_color);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_exhaust_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(exhaustColor), exhaustColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	perspectiveProjectionMatrix = mat4::identity();

	//Clear the Screen with OpenGL Color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Warmup call for resize
	resize(winWidth, winHeight);

	FullscreenToggle();

	return 0;
}

//Display Function
void display(void)
{
	//Declaration of Matrices
	mat4 translateMatrix;
	mat4 rotationMatirx;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	//Variable Declarations
	//Variable Declaration
	static GLfloat plane_x = -2.40f;
	static GLfloat translateILx = -2.0f;
	static GLfloat translateNy = 2.5f;
	static GLfloat translateIRy = -2.0f;
	static GLfloat translateAx = 2.0f;
	static GLfloat colorSaffron1 = 0.0f;
	static GLfloat colorSaffron2 = 0.0f;
	static GLfloat colorSaffron3 = 0.0f;
	static GLfloat colorGreen1 = 0.0f;
	static GLfloat colorGreen2 = 0.0f;
	static GLfloat colorGreen3 = 0.0f;
	static GLfloat plane_rotation = -90.0f;
	static GLsizei plane_radius = 0;
	static GLfloat exhaust_x1 = -0.75f, exhaust_x2 = -0.70f;
	GLsizei i = 1000;
	GLfloat plane_angle = (M_PI / 2) * plane_radius / i;
	static GLfloat plane_x2 = 0.0f;
	static GLsizei counter = 0.0f;
	GLfloat dColor[24], exhaust[18];

	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Left I
	translateMatrix = translate(translateILx, 0.0f, -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glLineWidth(3.0f);

	//Bind with vao for Left I
	glBindVertexArray(vao_il);

	//Similarly bind with Textures if any

	//Draw I
	glDrawArrays(GL_LINES, 0, 6);

	//Unbind with vao for Left I
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for N
	translateMatrix = translate(0.0f, translateNy, -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind with vao for N
	glBindVertexArray(vao_n);

	//Similarly bind with Textures if any

	//Draw N
	glDrawArrays(GL_LINES, 0, 6);

	//Unbind with vao for N
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for D
	translateMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	dColor[0] = colorSaffron1;
	dColor[1] = colorSaffron2;
	dColor[2] = colorSaffron3;
	dColor[3] = colorGreen1;
	dColor[4] = colorGreen2;
	dColor[5] = colorGreen2;
	dColor[6] = colorSaffron1;
	dColor[7] = colorSaffron2;
	dColor[8] = colorSaffron3;
	dColor[9] = colorSaffron1;
	dColor[10] = colorSaffron2;
	dColor[11] = colorSaffron3;
	dColor[12] = colorSaffron1;
	dColor[13] = colorSaffron2;
	dColor[14] = colorSaffron3;
	dColor[15] = colorGreen1;
	dColor[16] = colorGreen2;
	dColor[17] = colorGreen2;
	dColor[18] = colorGreen1;
	dColor[19] = colorGreen2;
	dColor[20] = colorGreen2;
	dColor[21] = colorGreen1;
	dColor[22] = colorGreen2;
	dColor[23] = colorGreen2;

	//Bind with vao for D
	glBindVertexArray(vao_d);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_d_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_DYNAMIC_DRAW);

	//Similarly bind with Textures if any

	//Draw D
	glDrawArrays(GL_LINES, 0, 8);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind with vao for D
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Right I
	translateMatrix = translate(0.0f, translateIRy, -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind with vao for Right I
	glBindVertexArray(vao_ir);

	//Similarly bind with Textures if any

	//Draw I
	glDrawArrays(GL_LINES, 0, 6);

	//Unbind with vao for Right I
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for A
	translateMatrix = translate(translateAx, 0.0f, -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind with vao for A
	glBindVertexArray(vao_a);

	//Similarly bind with Textures if any

	//Draw A
	glDrawArrays(GL_LINES, 0, 4);

	//Unbind with vao for A
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Triangle
	translateMatrix = translate(plane_x, 0.0f, -2.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind with vao for Plane
	glBindVertexArray(vao_plane);

	//Similarly bind with Textures if any

	//Draw Plane
	glDrawArrays(GL_TRIANGLES, 0, 15);

	//Unbind with vao for Plane
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	rotationMatirx = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Horizontal Plane
	translateMatrix = translate(-0.6f, 1.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;
	translateMatrix = translate((float)(-cos(plane_angle) + plane_x2), (float)(-sin(plane_angle)), -2.0f);
	rotationMatirx = rotate(plane_rotation, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix * rotationMatirx;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind with vao for Plane
	glBindVertexArray(vao_plane);

	//Similarly bind with Textures if any

	//Draw Plane
	glDrawArrays(GL_TRIANGLES, 0, 15);

	//Unbind with vao for Plane
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	rotationMatirx = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Planes
	translateMatrix = translate(-0.6f, -1.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;
	translateMatrix = translate((float)(-cos(plane_angle) + plane_x2), (float)(sin(plane_angle)), -2.0f);
	rotationMatirx = rotate(-plane_rotation, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix * rotationMatirx;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Bind with vao for Plane
	glBindVertexArray(vao_plane);

	//Similarly bind with Textures if any

	//Draw Plane
	glDrawArrays(GL_TRIANGLES, 0, 15);

	//Unbind with vao for Plane
	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	rotationMatirx = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for exhaust of the Planes
	translateMatrix = translate(0.0f, 0.0f, -2.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	if (plane_x >= -0.60f)
	{

		glLineWidth(5.0f);

		exhaust[0] = exhaust_x1;
		exhaust[1] = 0.01f;
		exhaust[2] = 0.0f;

		exhaust[3] = exhaust_x2;
		exhaust[4] = 0.01f;
		exhaust[5] = 0.0f;

		exhaust[6] = exhaust_x1;
		exhaust[7] = 0.0f;
		exhaust[8] = 0.0f;

		exhaust[9] = exhaust_x2;
		exhaust[10] = 0.0f;
		exhaust[11] = 0.0f;

		exhaust[12] = exhaust_x1;
		exhaust[13] = -0.01f;
		exhaust[14] = 0.0f;

		exhaust[15] = exhaust_x2;
		exhaust[16] = -0.01f;
		exhaust[17] = 0.0f;
	}

	//Bind with vao for Plane
	glBindVertexArray(vao_exhaust);

	//Similarly bind with Textures if any

	glBindBuffer(GL_ARRAY_BUFFER, vbo_exhaust_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(exhaust), exhaust, GL_DYNAMIC_DRAW);

	//Draw Plane
	glDrawArrays(GL_LINES, 0, 6);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind with vao for Plane
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);

	if (translateILx <= 0.0f)
	{
		translateILx = translateILx + 0.010f;
	}

	//Condition to translate A coming in from Right side
	if (translateILx >= 0.0f)
	{
		if (translateAx >= 00.f)
		{
			translateAx = translateAx - 0.010f;
		}
	}

	//Condition to translate N coming in from Top side
	if (translateAx <= 0.0f)
	{
		if (translateNy >= 0.0f)
		{
			translateNy = translateNy - 0.010f;
		}
	}

	//Condition to translate I coming in from Bottom side
	if (translateNy <= 0.0f)
	{
		if (translateIRy <= 0.0f)
		{
			translateIRy = translateIRy + 0.010f;
		}
	}

	//Condition to Fade In D using Color variables
	if (translateIRy >= 0.0f)
	{
		if (colorSaffron1 <= 1.0f)
		{
			colorSaffron1 = colorSaffron1 + 0.004f;
		}

		if (colorSaffron2 <= 0.60f)
		{
			colorSaffron2 = colorSaffron2 + 0.004f;
		}

		if (colorSaffron3 <= 0.20f)
		{
			colorSaffron3 = colorSaffron3 + 0.006f;
		}

		if (colorGreen1 <= 0.07f)
		{
			colorGreen1 = colorGreen1 + 0.020f;
		}

		if (colorGreen2 <= 0.53f)
		{
			colorGreen2 = colorGreen2 + 0.004f;
		}

		if (colorGreen3 <= 0.027f)
		{
			colorGreen3 = colorGreen3 + 0.020f;
		}
	}

	//Condition to translate the plane coming in Horizontally
	if (colorSaffron1 >= 1.0f)
	{
		if (plane_x <= 0.65f)
		{
			plane_x = plane_x + 0.00361f;
		}

		if (plane_radius < i)
		{
			plane_radius = plane_radius + 2;
			if (plane_rotation <= 0.0f)
			{
				plane_rotation = plane_rotation + 0.2f;
			}
		}

		if (counter <= 2000)
		{
			counter = counter + 1;
		}
	}

	//Condition to translate the Planes coming in from an angle to move Horizontally after the angle ends on the x-axis
	if (plane_radius >= i)
	{
		if (plane_x <= 0.65f)
		{
			plane_x2 = plane_x2 + 0.00361f;
		}
	}

	if (counter >= 2000)
	{

		if (plane_x <= 2.0f)
		{
			plane_x = plane_x + 0.00361f;
		}

		if (plane_radius < 2 * i)
		{
			plane_radius = plane_radius + 2;
		}

		if (plane_x > 0.65f)
		{
			plane_x2 = plane_x2 + 0.00005f;
		}

		plane_rotation = plane_rotation + 0.2f;

	}
	//Condition to start drawing the exhaust of Tri Color
	if (plane_x >= -0.60f)
	{
		if (exhaust_x2 <= 0.36f)
		{
			exhaust_x2 = exhaust_x2 + 0.00361f;
		}
	}

	//Condition to Fade out the Tri Color to a certain point on A
	if (exhaust_x2 >= 0.36f)
	{
		if (exhaust_x1 <= 0.27f)
		{
			exhaust_x1 = exhaust_x1 + 0.00361f;
		}
	}
}

//Resize Function
void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.f);
}

//Update Function
void update(void)
{
	//Code
}


//Unintialize Function
void uninitialize(void)
{
	//Code
	if (vbo_il_position)
	{
		glDeleteBuffers(1, &vbo_il_position);
		vbo_il_position = 0;
	}

	if (vbo_il_color)
	{
		glDeleteBuffers(1, &vbo_il_color);
		vbo_il_color = 0;
	}

	if (vao_il)
	{
		glDeleteVertexArrays(1, &vao_il);
		vao_il = 0;
	}

	if (vbo_n_position)
	{
		glDeleteBuffers(1, &vbo_n_position);
		vbo_n_position = 0;
	}

	if (vbo_n_color)
	{
		glDeleteBuffers(1, &vbo_n_color);
		vbo_n_color = 0;
	}

	if (vao_n)
	{
		glDeleteVertexArrays(1, &vao_n);
		vao_n = 0;
	}

	if (vbo_d_position)
	{
		glDeleteBuffers(1, &vbo_d_position);
		vbo_d_position = 0;
	}

	if (vbo_d_color)
	{
		glDeleteBuffers(1, &vbo_d_color);
		vbo_d_color = 0;
	}

	if (vao_d)
	{
		glDeleteVertexArrays(1, &vao_d);
		vao_d = 0;
	}

	if (vbo_ir_position)
	{
		glDeleteBuffers(1, &vbo_ir_position);
		vbo_ir_position = 0;
	}

	if (vbo_ir_color)
	{
		glDeleteBuffers(1, &vbo_ir_color);
		vbo_ir_color = 0;
	}

	if (vao_ir)
	{
		glDeleteVertexArrays(1, &vao_ir);
		vao_ir = 0;
	}

	if (vbo_a_position)
	{
		glDeleteBuffers(1, &vbo_a_position);
		vbo_a_position = 0;
	}

	if (vbo_a_color)
	{
		glDeleteBuffers(1, &vbo_a_color);
		vbo_a_color = 0;
	}

	if (vao_a)
	{
		glDeleteVertexArrays(1, &vao_a);
		vao_a = 0;
	}

	if (vbo_strips_position)
	{
		glDeleteBuffers(1, &vbo_strips_position);
		vbo_strips_position = 0;
	}

	if (vbo_strips_color)
	{
		glDeleteBuffers(1, &vbo_strips_color);
		vbo_strips_color = 0;
	}

	if (vao_strips)
	{
		glDeleteVertexArrays(1, &vao_strips);
		vao_strips = 0;
	}

	if (vbo_exhaust_position)
	{
		glDeleteBuffers(1, &vbo_exhaust_position);
		vbo_exhaust_position = 0;
	}

	if (vbo_exhaust_color)
	{
		glDeleteBuffers(1, &vbo_exhaust_color);
		vbo_exhaust_color = 0;
	}

	if (vao_exhaust)
	{
		glDeleteVertexArrays(1, &vao_exhaust);
		vao_exhaust = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		//Get Shader count from the program
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Detach Shaders one by one
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete detached Shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log file is Closed Successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}