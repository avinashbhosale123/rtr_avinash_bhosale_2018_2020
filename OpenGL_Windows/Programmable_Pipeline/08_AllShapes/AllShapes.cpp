#include<stdio.h>
#include<windows.h>
#include<GL/glew.h>
#include<gl/GL.h>
#include "vmath.h"

#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "glew32.lib")

using namespace vmath;

//Enum
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

//Global Variable Declaration
HWND ghwnd;
WINDOWPLACEMENT wpPrev;
DWORD dwStyle;
FILE *gbFile = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
int winWidth = 800;
int winHeight = 600;
bool gbDone = false;
bool gbFullscreen = false;
bool gbActive = false;
GLuint gShaderProgramObject;
GLuint vao_graph, vao_rectangle, vao_triangle, vao_inCircle, vao_outCircle;
GLuint vbo_position_graph, vbo_position_rectangle, vbo_position_triangle, vbo_position_inCircle, vbo_position_outCircle;
GLuint mvpUniform, color;
mat4 perspectiveProjectionMatrix;

//Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable Declaration
	int iRet;
	HWND hwnd;
	WNDCLASSEX wndclass;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyWindow");

	//Function Declaration
	int initialize(void);
	void display(void);
	void update(void);

	//Open the log file and Error Checking
	if (fopen_s(&gbFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Creation Failed"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gbFile, "Log File Created Successfully\n");
	}

	//Creating BluePrint for the window
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = szAppName;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszMenuName = NULL;

	//Registering the window class
	RegisterClassEx(&wndclass);

	//Creating the Window in Memory
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("My OpenGL Window - Avinash Bhosale"),
		WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		100,
		100,
		winWidth,
		winHeight,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//Call to Initialize function to Initialize OpenGL
	iRet = initialize();

	//Error checking for Initialization of  OpenGL
	if (iRet == -1)
	{
		fprintf(gbFile, "Choosing Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gbFile, "Set Pixel Format Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gbFile, "Create Context Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gbFile, "Make Current Context Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gbFile, "Initialization of OpenGL is successfull\n");
	}

	//Showing the Window
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				fprintf(gbFile, "Message = %d\n", msg.message);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == true)
			{
				update();
			}
			display();
		}
	}

	return((int)msg.wParam);
}

//CallBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void resize(int, int);
	void uninitialize(void);
	void FullscreenToggle(void);

	switch (iMsg)
	{

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			FullscreenToggle();
			break;
		}
		break;

	case WM_ERASEBKGND:
		return(0);
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_SETFOCUS:
		gbActive = true;
		break;

	case WM_KILLFOCUS:
		gbActive = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		winWidth = LOWORD(lParam);
		winHeight = HIWORD(lParam);
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

//Fullscreen Toggle Function
void FullscreenToggle(void)
{
	//Variable Decalaration
	MONITORINFO mi;

	//Code
	if (gbFullscreen == false)
	{

		mi = { sizeof(MONITORINFO) };

		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullscreen = true;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullscreen = false;
	}
}

//Initialize Function
int initialize(void)
{
	//Variable Declaration
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iProgramLinkStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	//Function Declaration
	void resize(int, int);
	void uninitialize(void);

	//Variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	//Code
	//Initializaton of PFD
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = { sizeof(PIXELFORMATDESCRIPTOR) };
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	result = glewInit();

	if (result != GLEW_OK)
	{
		uninitialize();
		DestroyWindow(ghwnd);
	}

	//Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	//Specify above source code to the vertex shader object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	//Compile the Vertex Shader
	glCompileShader(gVertexShaderObject);

	//Error Checking code
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Vertex Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Write Fragment Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform vec4 u_color;" \
		"void main(void)" \
		"{" \
		"FragColor = u_color;" \
		"}";

	//Specify above source code to the fragment shader object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	//Compile the Fragment Shader
	glCompileShader(gFragmentShaderObject);

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	//Error Checking code
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Fragment Shader Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Create Shader Program
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to the Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//Attach Fragment Shader to the Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Pre-Linking Binding to Vertex Attributes
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	//Link the Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error checking code
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gbFile, "Shader Program Info Log: \n%s", szInfoLog);
				free(szInfoLog);
				DestroyWindow(ghwnd);
				exit(0);
			}
		}
	}

	//Post Linking Retrieving of Uniform Locations
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	color = glGetUniformLocation(gShaderProgramObject, "u_color");

	GLfloat rectangleVertices[] = { 0.5f, 0.5f, 0.0f, -0.5f, 0.5f, 0.0f, 
									-0.5f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f,
									-0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f,
									0.5f, -0.5f, 0.0f, 0.5f, 0.5f, 0.0f };

	GLfloat triangleVertices[] = { 0.5f, -0.5f, 0.0f, 0.0f, 0.5f, 0.0f,
									0.0f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f,
									-0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f };

	//Create vao and vbo for Graph
	glGenVertexArrays(1, &vao_graph);
	glBindVertexArray(vao_graph);

	glGenBuffers(1, &vbo_position_graph);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_graph);
	glBufferData(GL_ARRAY_BUFFER, sizeof(6 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for rectangle
	glGenVertexArrays(1, &vao_rectangle);
	glBindVertexArray(vao_rectangle);

	glGenBuffers(1, &vbo_position_rectangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_rectangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Create vao and vbo for rectangle
	glGenVertexArrays(1, &vao_triangle);
	glBindVertexArray(vao_triangle);

	glGenBuffers(1, &vbo_position_triangle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &vao_inCircle);
	glBindVertexArray(vao_inCircle);

	//Create vao and vbo for Inner Circle
	glGenBuffers(1, &vbo_position_inCircle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_inCircle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(6 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//Create vao and vbo for Outer Circle
	glGenVertexArrays(1, &vao_outCircle);
	glBindVertexArray(vao_outCircle);

	glGenBuffers(1, &vbo_position_outCircle);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_outCircle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(6 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	perspectiveProjectionMatrix = mat4::identity();

	//Clear the Screen with OpenGL Color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Warmup call for resize
	resize(winWidth, winHeight);

	return 0;
}

//Display Function
void display(void)
{
	//Declaration of Matrices
	mat4 translateMatrix;
	mat4 rotationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	GLfloat graphVertices[6], outerCircleVertices[6], innerCircleVertices[6];
	GLfloat vColor[4];
	GLfloat length = 1.0f, angle;
	GLfloat radius, tside, tperimeter;

	vColor[0] = 0.0f;
	vColor[1] = 0.0f;
	vColor[2] = 1.0f;
	vColor[3] = 1.0f;

	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Triangle
	translateMatrix = translate(0.0f, 0.0f, -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform4fv(color, 1, vColor);

	//Bind with vao for Graph
	glBindVertexArray(vao_graph);

	//Similarly bind with Textures if any

	glLineWidth(1.0);

	//Draw the Graph
	for (float i = -1.0f; i < 1.05f; i = i + 0.05)
	{
		graphVertices[0] = i;
		graphVertices[1] = -1.0f;
		graphVertices[2] = 0.0f;
		graphVertices[3] = i;
		graphVertices[4] = 1.0f;
		graphVertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_graph);
		glBufferData(GL_ARRAY_BUFFER, sizeof(graphVertices), graphVertices, GL_DYNAMIC_DRAW);

		glDrawArrays(GL_LINES, 0, 2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	for (float i = -1.0f; i < 1.05f; i = i + 0.05)
	{
		graphVertices[0] = -1.0f;
		graphVertices[1] = i;
		graphVertices[2] = 0.0f;
		graphVertices[3] = 1.0f;
		graphVertices[4] = i;
		graphVertices[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_graph);
		glBufferData(GL_ARRAY_BUFFER, sizeof(graphVertices), graphVertices, GL_DYNAMIC_DRAW);

		glDrawArrays(GL_LINES, 0, 2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	glDrawArrays(GL_LINES, 0, 164);

	//Unbind with vao for Graph
	glBindVertexArray(0);

	vColor[0] = 1.0f;
	vColor[1] = 1.0f;
	vColor[2] = 0.0f;
	vColor[3] = 1.0f;

	glUniform4fv(color, 1, vColor);

	glLineWidth(3.0);

	//Draw Rectangle
	glBindVertexArray(vao_rectangle);

	glDrawArrays(GL_LINES, 0, 8);

	glBindVertexArray(0);

	//Draw Triangle
	glBindVertexArray(vao_triangle);

	glDrawArrays(GL_LINES, 0, 6);

	glBindVertexArray(0);

	//Formula for radius of the outer circle
	radius = sqrt((length / 2 * length / 2) + ((length / 2) * (length / 2)));

	//Drawing Outer Circle
	glBindVertexArray(vao_outCircle);

	for (angle = 0.0f; angle < (2 * M_PI); angle = angle + 0.01f)
	{
		int i = 0;

		outerCircleVertices[i] = radius * cos(angle);
		outerCircleVertices[i + 1] = radius * sin(angle);
		outerCircleVertices[i + 2] = 0.0;

		outerCircleVertices[i + 3] = radius * cos(angle + 0.01);
		outerCircleVertices[i + 4] = radius * sin(angle + 0.01);
		outerCircleVertices[i + 5] = 0.0;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_outCircle);
		glBufferData(GL_ARRAY_BUFFER, sizeof(outerCircleVertices), outerCircleVertices, GL_DYNAMIC_DRAW);

		glDrawArrays(GL_LINES, 0, 2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		i = i + 3;
	}

	glBindVertexArray(0);

	//Initialize matrices to identity
	translateMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	//Do necessary transformation for Triangle
	translateMatrix = translate(0.0f, -((length / 2.0f) - (length / 3.20f)), -3.0f);
	modelViewMatrix = modelViewMatrix * translateMatrix;

	//Do necessary matrix multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to Shader in respective Uniforms
	glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	//Calcuting the side of the trinagle using pythagoras theorem
	tside = sqrt((length * length) + ((length / 2) * (length / 2)));

	//Triangle perimeter
	tperimeter = (tside + tside + length) / 2;

	//Formula for In-circle radius of the triangle
	radius = sqrt(((tperimeter - tside) * (tperimeter - tside) * (tperimeter - length)) / tperimeter);

	//Drawing Inner Circle
	glBindVertexArray(vao_inCircle);

	for (angle = 0.0f; angle < (2 * M_PI); angle = angle + 0.01f)
	{
		int i = 0;

		innerCircleVertices[i] = radius * cos(angle);
		innerCircleVertices[i + 1] = radius * sin(angle);
		innerCircleVertices[i + 2] = 0.0;

		innerCircleVertices[i + 3] = radius * cos(angle + 0.01);
		innerCircleVertices[i + 4] = radius * sin(angle + 0.01);
		innerCircleVertices[i + 5] = 0.0;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_inCircle);
		glBufferData(GL_ARRAY_BUFFER, sizeof(innerCircleVertices), innerCircleVertices, GL_DYNAMIC_DRAW);

		glDrawArrays(GL_LINES, 0, 2);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		i = i + 3;
	}

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);
}

//Resize Function
void resize(int width, int height)
{
	//Code
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.f);
}

//Update Function
void update(void)
{
	//Code
}


//Unintialize Function
void uninitialize(void)
{
	//Code
	if (vbo_position_graph)
	{
		glDeleteBuffers(1, &vbo_position_graph);
		vbo_position_graph = 0;
	}

	if (vbo_position_outCircle)
	{
		glDeleteBuffers(1, &vbo_position_outCircle);
		vbo_position_outCircle = 0;
	}

	if (vbo_position_inCircle)
	{
		glDeleteBuffers(1, &vbo_position_inCircle);
		vbo_position_inCircle = 0;
	}

	if (vbo_position_rectangle)
	{
		glDeleteBuffers(1, &vbo_position_rectangle);
		vbo_position_rectangle = 0;
	}

	if (vbo_position_triangle)
	{
		glDeleteBuffers(1, &vbo_position_triangle);
		vbo_position_triangle = 0;
	}

	if (vao_graph)
	{
		glDeleteVertexArrays(1, &vao_graph);
		vao_graph = 0;
	}

	if (vao_outCircle)
	{
		glDeleteVertexArrays(1, &vao_outCircle);
		vao_outCircle = 0;
	}

	if (vao_inCircle)
	{
		glDeleteVertexArrays(1, &vao_inCircle);
		vao_inCircle = 0;
	}

	if (vao_rectangle)
	{
		glDeleteVertexArrays(1, &vao_rectangle);
		vao_rectangle = 0;
	}

	if (vao_triangle)
	{
		glDeleteVertexArrays(1, &vao_triangle);
		vao_triangle = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		//Get Shader count from the program
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);
		GLuint *pShaders = (GLuint *)malloc(sizeof(GLuint) * shaderCount);
		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Detach Shaders one by one
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete detached Shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullscreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gbFile)
	{
		fprintf(gbFile, "Log file is Closed Successfully\n");
		fclose(gbFile);
		gbFile = NULL;
	}
}