package com.rtr.ogl_ortho;

//Added packages by me
import android.content.Context; //For context
import android.graphics.Color; //For Color
import android.view.Gravity; //For Gravity
import android.view.MotionEvent; // for MotionEvent
import android.view.GestureDetector; // for GestureDetector
import android.view.GestureDetector.OnGestureListener; // for OnGestureListener
import android.view.GestureDetector.OnDoubleTapListener; // OnDoubleTapListener
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
    private GestureDetector gestureDetector;
    private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	
	private int mvpUniform;
	
	private float[] orthographicProjectionMatrix = new float[16]; //4X4 Matrix

    public GLESView(Context drawingContext)
    {
	super(drawingContext);
	context = drawingContext;

	setEGLContextClientVersion(3);
	setRenderer(this);
	setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

	gestureDetector = new GestureDetector(context, this, null, false);
	gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventaction = event.getAction();
        if(!gestureDetector.onTouchEvent(event))
	{
            super.onTouchEvent(event);
	}
        return(true);
    }

    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        // Do not write any code here because it is already written in 'onDoubleTap'
        return(true);
    }
    
    // abstract method from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)
    {
        // Do not write any code here because it is already written in 'onSingleTapConfirmed'
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
		uninitialize();
		System.exit(0);
        return(true);
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)
    {
    }
    
    // abstract method from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    //Implement GLSurfaceView.Renderer Methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: "+version);
		version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: "+version);
		version = gl.glGetString(GLES32.GL_VENDOR);
		System.out.println("RTR: "+version);
		version = gl.glGetString(GLES32.GL_RENDERER);
		System.out.println("RTR: "+version);
		initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
		resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
		display();
    }

    //Creating our custom methods
    private void initialize()
    {
		//Vertex Shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
		"uniform mat4 u_mvp_matrix;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		
		//Compile error checkign
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		
		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		//The zero in the last parameter represents 0th Element address
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("RTR: VERTEX SHADER COMPILE STATUS: "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//Fragment Shader
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = vec4(1.0, 1.0, 0.0, 1.0);" +
		"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		
		//Compile error checkign
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
		//The zero in the last parameter represents 0th Element address
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("RTR: FRAGMENT SHADER COMPILE STATUS: "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//Shader Program Object
		shaderProgramObject = GLES32.glCreateProgram();
		
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//Prelink binding of Attributes
		GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		
		//Linking
		GLES32.glLinkProgram(shaderProgramObject);
		
		//Linking error checking write based on the compile error cehcking
		int[] iProgramLinkStatus = new int[1];
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);

		if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if (iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("RTR: SHADER PROGRAM LINK STATUS: "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		//Get Uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
		
		//Triangle Position Vertices
		final float[] triangleVertices = new float[]
											{
												 0.0f, 50.f, 0.0f, -50.0f, -50.0f, 0.0f, 50.0f, -50.0f, 0.0f	
											};	
		
		//Create vao
		GLES32.glGenVertexArrays(1, vao, 0);
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glGenBuffers(1, vbo, 0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);
		
		//Convert the array into a buffer which we can pass to GLBufferData
		
		//Allocate the buffer directly from native memory(Not VM memory)
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4); //4 is size of float
		
		//Arrange the buffer into native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		
		//Create the float type and convert our byteBuffer into float buffer
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
		
		//Now put your array at into this "Cooked Buffer"
		positionBuffer.put(triangleVertices);
		
		//Set the array at the 0th position of the Buffer
		positionBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, triangleVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION, 3, GLES32.GL_FLOAT, false, 0, 0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		GLES32.glBindVertexArray(0);
		
		//Include Depth Lines
		
		Matrix.setIdentityM(orthographicProjectionMatrix, 0);
		
		GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
		
		//No warm up call for resize
    }

    private void resize(int width, int height)
    {
		//Code
		if (height == 0)
		{
			height = 1;
		}

		GLES32.glViewport(0, 0, width, height);

		if (width < height)
		{
			Matrix.orthoM(orthographicProjectionMatrix, 0, -100.0f, 100.0f, (-100.0f * height / width), (100.0f * height / width), -100.0f, 100.0f);
		}
		else
		{
			Matrix.orthoM(orthographicProjectionMatrix, 0, (-100.0f * width / height), (100.0f * width / height), -100.0f, 100.0f, -100.0f, 100.0f);
		}
	}

    private void display()
    {
		//Declaration of Matrices
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		//Code
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);

		//Initialize matrices to identity
		Matrix.setIdentityM(modelViewMatrix, 0);
		Matrix.setIdentityM(modelViewProjectionMatrix, 0);

		//Do necessary transformation

		//Do necessary matrix multiplication
		Matrix.multiplyMM(modelViewProjectionMatrix, 0, orthographicProjectionMatrix, 0, modelViewProjectionMatrix, 0);

		//Send necessary matrices to Shader in respective Uniforms
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

		//Bind with vao
		GLES32.glBindVertexArray(vao[0]);

		//Similarly bind with Textures if any

		//Draw the necessary scene
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);

		//Unbind with vao
		GLES32.glBindVertexArray(0);

		GLES32.glUseProgram(0);
		requestRender();
    }

	private void uninitialize()
	{
		//Code
		if (vbo[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo, 0);
			vbo[0] = 0;
		}

		if (vao[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao, 0);
			vao[0] = 0;
		}

		if (shaderProgramObject != 0)
		{
			int[] shaderCount = new int[1];
			int[] pShaders = new int[1];
			int shaderNumber;

			GLES32.glUseProgram(shaderProgramObject);

			//Get Shader count from the program
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);
			GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, pShaders, 0);
			for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
			{
				//Detach Shaders one by one
				GLES32.glDetachShader(shaderProgramObject, pShaders[shaderNumber]);

				//Delete detached Shader
				GLES32.glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}

			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
	}
}
    