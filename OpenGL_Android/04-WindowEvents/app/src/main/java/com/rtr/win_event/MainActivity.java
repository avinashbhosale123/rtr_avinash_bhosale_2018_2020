package com.rtr.win_event;

//default given packages by Android
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

//Added packages by me
import android.view.Window; //For window class
import android.view.WindowManager; //For window manager class
import android.content.pm.ActivityInfo; //For activityinfo class
import android.graphics.Color; //For color class

public class MainActivity extends AppCompatActivity 
{
    private MyView myView;

    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

	//Get rid of Title Bar
	this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	this.getSupportActionBar().hide();

	//Make Fullscreen
	this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

	//Do forced Landscape Orientation
	this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	//Set Background Color
	this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

	//Define our own View
	myView = new MyView(this);

	//Now set this View as our main View
	setContentView(myView);
    }

    protected void onPause()
    {
	super.onPause();
    }
    
    protected void onResme()
    {
	super.onResume();
    }

}

