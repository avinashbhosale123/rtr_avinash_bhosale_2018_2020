//Global Variables
var canvas_ab=null;
var gl=null;
var bFullscreen_ab=false;
var canvas_original_width_ab;
var canvas_original_height_ab;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_ab;
var fragmentShaderObject_ab;
var shaderProgramObject_ab;

var vao_triangle_ab, vao_circle_ab, vao_line_ab;
var vbo_triangle_ab, vbo_circle_ab, vbo_line_ab;
var mvpuniform;
var perspectiveProjectionMatrix;
var rotation_angle = 0.0;
var triangle_x = -1.0;
var triangle_y = -1.0;
var circle_x = -1.0;
var circle_y = -1.0;
var line_y = 1.0;

//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get Canvas element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	canvas_original_width_ab=canvas_ab.width;
	canvas_original_height_ab=canvas_ab.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas_ab.requestFullscreen)
			canvas_ab.requestFullscreen();
		else if(canvas_ab.mozRequestFullScreen)
			canvas_ab.mozRequestFullScreen();
		else if (canvas_ab.webkitRequestFullscreen)
			canvas_ab.webkitRequestFullscreen();
		else if(canvas_ab.msRequestFullscreen)
			canvas_ab.msRequestFullscreen();
		bFullscreen_ab=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen_ab=false;
	}
}

function init()
{
    //Code
    //Variable Declaration
    var i;
    var num_points = 1000;
    var length = 0.8;
    var radius, height, x, y;

    height = (Math.sqrt(3) / 2) * length;
    radius = (Math.sqrt(3) / 6) * length;

	//Get the WebGL 2.0 Context
	gl = canvas_ab.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas_ab.width;
	gl.viewpoetHeight = canvas_ab.height;

	//Vertex Shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"}";
	
	vertexShaderObject_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_ab, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject_ab);
	
	if(gl.getShaderParameter(vertexShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(vertexShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
    "}";
	
	fragmentShaderObject_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ab, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject_ab);
	
	if(gl.getShaderParameter(fragmentShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(fragmentShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Shader Program
	shaderProgramObject_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_ab, vertexShaderObject_ab);
	gl.attachShader(shaderProgramObject_ab, fragmentShaderObject_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	
	//Linking
	gl.linkProgram(shaderProgramObject_ab);
	
	if(gl.getProgramParameter(shaderProgramObject_ab, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Get MVP Uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_mvp_matrix");

	var triangleVertices = new Float32Array([ 0.0, height - radius, 0.0, -(length / 2), -radius, 0.0,
									-(length / 2), -radius, 0.0, length / 2, -radius, 0.0,
									length / 2, -radius, 0.0, 0.0, height - radius, 0.0]);

	var lineVertices = new Float32Array([0.0, height - radius, 0.0, 0.0, -radius, 0.0]);

	var circleVertices = [];

	var j = 0;

    for (i = 0; i < num_points; i++)
    {
        angle = (2 * Math.PI * i) / num_points;

        var x = radius * Math.cos(angle);
        var y = radius * Math.sin(angle);
        var z = 0.0;

        circleVertices.push(x);
        circleVertices.push(y);
        circleVertices.push(z);
    }

	//Vao for Triangle
	vao_triangle_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_triangle_ab);

	vbo_triangle_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_triangle_ab);
	gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_line_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_line_ab);

	vbo_line_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_line_ab);
	gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_circle_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_circle_ab);

	vbo_circle_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_circle_ab);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(circleVertices), gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen_ab == true)
	{
		canvas_ab.width=window.innerWidth;
		canvas_ab.height=window.innerHeight;
	}
	else
	{
		canvas_ab.width=canvas_original_width_ab;
		canvas_ab.height=canvas_original_height_ab;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas_ab.width, canvas_ab.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas_ab.width)/parseFloat(canvas_ab.height), 0.1, 100.0);
	
}

function draw()
{
	//Code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject_ab);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	triangle_x = triangle_x + 0.005;
	triangle_y = triangle_y + 0.005;

	if (triangle_x >= 0.0 && triangle_y >= 0.0)
	{
	    triangle_x = 0.0;
	    triangle_y = 0.0;
	}

	mat4.translate(modelViewMatrix, modelViewMatrix, [triangle_x, triangle_y, -3.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(rotation_angle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vao_triangle_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	if (triangle_x == 0.0 && triangle_y == 0.0)
	{
	    circle_x = circle_x + 0.005;
	    circle_y = circle_y + 0.005;

	    if (circle_x >= 0.0)
	    {
	        circle_x = 0.0;
	    }

	    if (circle_y >= 0.0)
	    {
	        circle_y = 0.0;
	    }

	    mat4.identity(modelViewMatrix);
	    mat4.identity(modelViewProjectionMatrix);

	    mat4.translate(modelViewMatrix, modelViewMatrix, [-circle_x, circle_y, -3.0]);
	    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(rotation_angle));

	    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	    gl.bindVertexArray(vao_circle_ab);

	    gl.drawArrays(gl.LINES, 0, 1000);

	    gl.bindVertexArray(null);
	}

	if (circle_x == 0.0 && circle_y == 0.0)
	{

	    line_y = line_y - 0.1;

	    if (line_y <= 0.0)
	    {
	        line_y = 0.0;
	    }

	    mat4.identity(modelViewMatrix);
	    mat4.identity(modelViewProjectionMatrix);

	    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, line_y, -3.0]);

	    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	    gl.bindVertexArray(vao_line_ab);

	    gl.drawArrays(gl.LINES, 0, 2);

	    gl.bindVertexArray(null);

	}

	gl.useProgram(null);

	update();

	//Animation Loop
	requestAnimationFrame(draw, canvas_ab);
}

function update()
{
    //Code
    rotation_angle = rotation_angle + 5.0;

    if (rotation_angle >= 360.0)
    {
        rotation_angle = 0.0;
    }
}

function uninitialize()
{
	//Code
	if(vao_triangle_ab)
	{
	    gl.deleteVertexArray(vao_triangle_ab);
	    vao_triangle_ab = null;
	}

	if(vbo_triangle_ab)
	{
	    gl.deleteBuffer(vbo_triangle_ab);
	    vbo_triangle_ab = null;
	}

	if (vao_circle_ab)
	{
	    gl.deleteVertexArray(vao_circle_ab);
	    vao_circle_ab = null;
	}

	if (vbo_circle_ab)
	{
	    gl.deleteBuffer(vbo_circle_ab);
	    vbo_circle_ab = null;
	}

	if (vao_line_ab)
	{
	    gl.deleteVertexArray(vao_line_ab);
	    vao_line_ab = null;
	}

	if (vbo_line_ab)
	{
	    gl.deleteBuffer(vbo_line_ab);
	    vbo_line_ab = null;
	}

	if(shaderProgramObject_ab)
	{
		if(fragmentShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, fragmentShaderObject_ab);
			gl.deleteShader(fragmentShaderObject_ab);
			fragmentShaderObject_ab = null;
		}

		if(vertexShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, vertexShaderObject_ab);
			gl.deleteShader(vertexShaderObject_ab);
			vertexShaderObject_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_ab);
		shaderProgramObject_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{

		case 27:
			//Escape for uninitialize
			uninitialize();
			//Close our Application Tab
			window.close();
			break;

		case 70: // For 'F' or 'f'
			toggleFullscreen();
			break;
	}
}

function mouseDown()
{
	//Code
}

function degToRad(degrees)
{
    return (degrees * Math.PI / 180);
}