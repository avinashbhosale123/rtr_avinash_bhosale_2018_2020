//Global Variables
var canvas_ab=null;
var gl=null;
var bFullscreen_ab=false;
var canvas_original_width_ab;
var canvas_original_height_ab;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_ab;
var fragmentShaderObject_ab;
var shaderProgramObject_ab;

var vao_graph_ab;
var vbo_graph;
var mvpuniform;
var perspectiveProjectionMatrix;
var graphVertices_ab = new Float32Array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);

//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get canvas_ab element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	canvas_original_width_ab=canvas_ab.width;
	canvas_original_height_ab=canvas_ab.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas_ab.requestFullscreen)
			canvas_ab.requestFullscreen();
		else if(canvas_ab.mozRequestFullScreen)
			canvas_ab.mozRequestFullScreen();
		else if (canvas_ab.webkitRequestFullscreen)
			canvas_ab.webkitRequestFullscreen();
		else if(canvas_ab.msRequestFullscreen)
			canvas_ab.msRequestFullscreen();
		bFullscreen_ab=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen_ab=false;
	}
}

function init()
{
	//Code
	//Get the WebGL 2.0 Context
	gl = canvas_ab.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas_ab.width;
	gl.viewpoetHeight = canvas_ab.height;

	//Vertex Shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"}";
	
	vertexShaderObject_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_ab, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject_ab);
	
	if(gl.getShaderParameter(vertexShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(vertexShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"FragColor = vec4(0.0, 0.0, 1.0, 1.0);" +
	"}";
	
	fragmentShaderObject_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ab, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject_ab);
	
	if(gl.getShaderParameter(fragmentShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(fragmentShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Shader Program
	shaderProgramObject_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_ab, vertexShaderObject_ab);
	gl.attachShader(shaderProgramObject_ab, fragmentShaderObject_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
	
	//Linking
	gl.linkProgram(shaderProgramObject_ab);
	
	if(gl.getProgramParameter(shaderProgramObject_ab, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Get MVP Uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_mvp_matrix");

	var graphVertices_ab = new Float32Array([-1.0, -1.0, 0.0, 1.0, -1.0, 0.0, -1.0, -1.0, 0.0, -1.0, 1.0, 0.0,
									-1.0, -0.95, 0.0, 1.0, -0.95, 0.0, -0.95, -1.0, 0.0, -0.95, 1.0, 0.0,
									-1.0, -0.90, 0.0, 1.0, -0.90, 0.0, -0.90, -1.0, 0.0, -0.90, 1.0, 0.0,
									-1.0, -0.85, 0.0, 1.0, -0.85, 0.0, -0.85, -1.0, 0.0, -0.85, 1.0, 0.0,
									-1.0, -0.80, 0.0, 1.0, -0.80, 0.0, -0.80, -1.0, 0.0, -0.80, 1.0, 0.0,
									-1.0, -0.75, 0.0, 1.0, -0.75, 0.0, -0.75, -1.0, 0.0, -0.75, 1.0, 0.0,
									-1.0, -0.70, 0.0, 1.0, -0.70, 0.0, -0.70, -1.0, 0.0, -0.70, 1.0, 0.0,
									-1.0, -0.65, 0.0, 1.0, -0.65, 0.0, -0.65, -1.0, 0.0, -0.65, 1.0, 0.0,
									-1.0, -0.60, 0.0, 1.0, -0.60, 0.0, -0.60, -1.0, 0.0, -0.60, 1.0, 0.0,
									-1.0, -0.55, 0.0, 1.0, -0.55, 0.0, -0.55, -1.0, 0.0, -0.55, 1.0, 0.0,
									-1.0, -0.50, 0.0, 1.0, -0.50, 0.0, -0.50, -1.0, 0.0, -0.50, 1.0, 0.0,
									-1.0, -0.45, 0.0, 1.0, -0.45, 0.0, -0.45, -1.0, 0.0, -0.45, 1.0, 0.0,
									-1.0, -0.40, 0.0, 1.0, -0.40, 0.0, -0.40, -1.0, 0.0, -0.40, 1.0, 0.0,
									-1.0, -0.35, 0.0, 1.0, -0.35, 0.0, -0.35, -1.0, 0.0, -0.35, 1.0, 0.0,
									-1.0, -0.30, 0.0, 1.0, -0.30, 0.0, -0.30, -1.0, 0.0, -0.30, 1.0, 0.0,
									-1.0, -0.25, 0.0, 1.0, -0.25, 0.0, -0.25, -1.0, 0.0, -0.25, 1.0, 0.0,
									-1.0, -0.20, 0.0, 1.0, -0.20, 0.0, -0.20, -1.0, 0.0, -0.20, 1.0, 0.0,
									-1.0, -0.15, 0.0, 1.0, -0.15, 0.0, -0.15, -1.0, 0.0, -0.15, 1.0, 0.0,
									-1.0, -0.10, 0.0, 1.0, -0.10, 0.0, -0.10, -1.0, 0.0, -0.10, 1.0, 0.0,
									-1.0, -0.05, 0.0, 1.0, -0.05, 0.0, -0.05, -1.0, 0.0, -0.05, 1.0, 0.0,
									-1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0,
									-1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, -1.0, 0.0, 1.0, 1.0, 0.0,
									-1.0, 0.95, 0.0, 1.0, 0.95, 0.0, 0.95, -1.0, 0.0, 0.95, 1.0, 0.0,
									-1.0, 0.90, 0.0, 1.0, 0.90, 0.0, 0.90, -1.0, 0.0, 0.90, 1.0, 0.0,
									-1.0, 0.85, 0.0, 1.0, 0.85, 0.0, 0.85, -1.0, 0.0, 0.85, 1.0, 0.0,
									-1.0, 0.80, 0.0, 1.0, 0.80, 0.0, 0.80, -1.0, 0.0, 0.80, 1.0, 0.0,
									-1.0, 0.75, 0.0, 1.0, 0.75, 0.0, 0.75, -1.0, 0.0, 0.75, 1.0, 0.0,
									-1.0, 0.70, 0.0, 1.0, 0.70, 0.0, 0.70, -1.0, 0.0, 0.70, 1.0, 0.0,
									-1.0, 0.65, 0.0, 1.0, 0.65, 0.0, 0.65, -1.0, 0.0, 0.65, 1.0, 0.0,
									-1.0, 0.60, 0.0, 1.0, 0.60, 0.0, 0.60, -1.0, 0.0, 0.60, 1.0, 0.0,
									-1.0, 0.55, 0.0, 1.0, 0.55, 0.0, 0.55, -1.0, 0.0, 0.55, 1.0, 0.0,
									-1.0, 0.50, 0.0, 1.0, 0.50, 0.0, 0.50, -1.0, 0.0, 0.50, 1.0, 0.0,
									-1.0, 0.45, 0.0, 1.0, 0.45, 0.0, 0.45, -1.0, 0.0, 0.45, 1.0, 0.0,
									-1.0, 0.40, 0.0, 1.0, 0.40, 0.0, 0.40, -1.0, 0.0, 0.40, 1.0, 0.0,
									-1.0, 0.35, 0.0, 1.0, 0.35, 0.0, 0.35, -1.0, 0.0, 0.35, 1.0, 0.0,
									-1.0, 0.30, 0.0, 1.0, 0.30, 0.0, 0.30, -1.0, 0.0, 0.30, 1.0, 0.0,
									-1.0, 0.25, 0.0, 1.0, 0.25, 0.0, 0.25, -1.0, 0.0, 0.25, 1.0, 0.0,
									-1.0, 0.20, 0.0, 1.0, 0.20, 0.0, 0.20, -1.0, 0.0, 0.20, 1.0, 0.0,
									-1.0, 0.15, 0.0, 1.0, 0.15, 0.0, 0.15, -1.0, 0.0, 0.15, 1.0, 0.0,
									-1.0, 0.10, 0.0, 1.0, 0.10, 0.0, 0.10, -1.0, 0.0, 0.10, 1.0, 0.0,
									-1.0, 0.05, 0.0, 1.0, 0.05, 0.0, 0.05, -1.0, 0.0, 0.05, 1.0, 0.0]);

	//Vao for Graph
	vao_graph_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_graph_ab);

	vbo_graph = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_graph);
	gl.bufferData(gl.ARRAY_BUFFER, graphVertices_ab, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen_ab == true)
	{
		canvas_ab.width=window.innerWidth;
		canvas_ab.height=window.innerHeight;
	}
	else
	{
		canvas_ab.width=canvas_original_width_ab;
		canvas_ab.height=canvas_original_height_ab;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas_ab.width, canvas_ab.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas_ab.width)/parseFloat(canvas_ab.height), 0.1, 100.0);
	
}

function draw()
{
	//Code
	//Variable Declaration
	var i;

	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject_ab);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vao_graph_ab);

	gl.drawArrays(gl.LINES, 0, 164);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	//Animation Loop
	requestAnimationFrame(draw, canvas_ab);
}

function update()
{
    //Code
}

function uninitialize()
{
	//Code
	if(vao_graph_ab)
	{
	    gl.deleteVertexArray(vao_graph_ab);
	    vao_graph_ab = null;
	}

	if(vbo_graph)
	{
	    gl.deleteBuffer(vbo_graph);
	    vbo_graph = null;
	}

	if(shaderProgramObject_ab)
	{
		if(fragmentShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, fragmentShaderObject_ab);
			gl.deleteShader(fragmentShaderObject_ab);
			fragmentShaderObject_ab = null;
		}

		if(vertexShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, vertexShaderObject_ab);
			gl.deleteShader(vertexShaderObject_ab);
			vertexShaderObject_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_ab);
		shaderProgramObject_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{

		case 27:
			//Escape for uninitialize
			uninitialize();
			//Close our Application Tab
			window.close();
			break;

		case 70: // For 'F' or 'f'
			toggleFullscreen();
			break;
	}
}

function mouseDown()
{
	//Code
}