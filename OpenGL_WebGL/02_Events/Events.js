//Global Variables
var canvas_ab=null;
var context_ab=null;

//Onload function
function main()
{
	//Get canvas element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	//print Canvas width and height on console
	console.log("Canvas Width :"+canvas_ab.width+" and Canvas Height :"+canvas_ab.height);

	//Get Ordinary 2D Context from the Canvas
	context_ab = canvas_ab.getContext("2d");
	if(!context_ab)
		console.log("Obtaining 2D Context Failed\n");
	else
		console.log("Obtaining 2D Context Succeeded\n");

	//Fill Canvas with Black color
	context_ab.fillStyle="black"; //"#000000"
	context_ab.fillRect(0,0,canvas_ab.width,canvas_ab.height);

	//Center the Text
	context_ab.textAlign="center"; //Center Horizontally
	context_ab.textBaseline="middle"; //Center Vertically

	//Text
	var str="Hello World!!";

	//Text Font
	context_ab.font="48px sans-serif";
	
	//Text Color
	context_ab.fillStyle="white"; //"#FFFFFF"

	//Display the Text in Center
	context_ab.fillText(str,canvas_ab.width/2,canvas_ab.height/2);

	//Register keyboard's keydown event handler
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
}

function keyDown(event)
{
	//Code
	alert("A Key is Pressed");
}

function mouseDown()
{
	//Code
	alert("Mouse is Clicked");
}