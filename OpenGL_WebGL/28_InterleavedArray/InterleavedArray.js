//Global Variables
var canvas=null;
var gl=null;
var bFullscreen=false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_ab;
var fragmentShaderObject_ab;
var shaderProgramObject_ab;

var vao_cube_ab;
var vbo_cube_ab;
var marble_texture_ab = 0;
var mUniform, vUniform, pUniform;
var perspectiveProjectionMatrix;
var samplerUniform_ab;

var laUniform_ab, lsUniform_ab, ldUniform_ab, lightPositionUniform_ab;
var kaUniform_ab, ksUniform_ab, kdUniform_ab, materialShininessUniform_ab;
var LKeyPressedUniform_ab;

var lKeyPressed = false;
var angle_cube = 0.0;

var lightAmbient_ab = [ 0.25, 0.25, 0.25];
var lightDiffuse_ab = [ 1.0, 1.0, 1.0 ];
var lightSpecular_ab = [ 1.0, 1.0, 1.0 ];
var lightPosition_ab = [ 1.0, 1.0, 1.0, 1.0 ];
var materialAmbient_ab = [ 0.0, 0.0, 0.0 ];
var materialDiffuse_ab = [ 1.0, 1.0, 1.0 ] ;
var materialSpecular_ab = [ 1.0, 1.0, 1.0 ];
var materialShininess_ab = 50.0;

//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	canvas_original_width=canvas.width;
	canvas_original_height=canvas.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if (canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullscreen=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen=false;
	}
}

function init()
{
	//Code
	//Get the WebGL 2.0 Context
	gl = canvas.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas.width;
	gl.viewpoetHeight = canvas.height;

	//Vertex Shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
    "\n" +
    "in vec4 vPosition;" + 
    "in vec3 vColor;" +
    "in vec3 vNormal;" +
    "in vec2 vTexture;" +
    "uniform mat4 u_m_matrix;" +
    "uniform mat4 u_v_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform vec4 u_light_position;" +
    "out vec3 tNorm;" +
    "out vec3 lightDirection;" +
    "out vec3 viewerVector;" +
    "out vec3 out_color;" +
    "out vec2 out_texcoord;" +
    "void main(void)" +
    "{" +
    "vec4 eyeCoordinates = u_v_matrix * u_m_matrix * vPosition;" +
    "tNorm = mat3(u_v_matrix * u_m_matrix) * vNormal;" +
    "lightDirection = vec3(u_light_position - eyeCoordinates);" +
    "viewerVector = vec3(-eyeCoordinates);" +
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
    "out_color = vColor;" +
    "out_texcoord = vTexture;" +
    "}";
	
	vertexShaderObject_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_ab, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject_ab);
	
	if(gl.getShaderParameter(vertexShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
    "\n" +
    "precision highp float;" +
    "uniform vec3 u_la;" +
    "uniform vec3 u_ld;" +
    "uniform vec3 u_ls;" +
    "uniform vec3 u_ka;" +
    "uniform vec3 u_kd;" +
    "uniform vec3 u_ks;" +
    "uniform sampler2D u_sampler;" +
    "uniform float u_material_shininess;" +
    "in vec3 tNorm;" +
    "in vec3 lightDirection;" +
    "in vec3 viewerVector;" +
    "in vec3 out_color;" +
    "in vec2 out_texcoord;" +
    "uniform mediump int u_lIsPressed;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "if(u_lIsPressed==1)" +
    "{" +
    "vec3 normalizedTNorm = normalize(tNorm);" +
    "vec3 normalizedLightDirection = normalize(lightDirection);" +
    "vec3 normalizedViewerVector = normalize(viewerVector);" +
    "float tn_dot_ld = max(dot(normalizedLightDirection, normalizedTNorm), 0.0);" +
    "vec3 reflectionVector = reflect(-normalizedLightDirection, normalizedTNorm);" +
    "vec3 ambient = u_la * u_ka;" +
    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" +
    "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalizedViewerVector), 0.0), u_material_shininess);" +
    "vec3 phong_ads_light = ambient + diffuse + specular;" +
    "vec4 tex = texture(u_sampler, out_texcoord);" +
    "FragColor = vec4((vec3(tex) * vec3(out_color) * phong_ads_light), 1.0);" +
    "}" +
    "else" +
    "{" +
    "FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
    "}" +
    "}";
	
	fragmentShaderObject_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ab, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject_ab);
	
	if(gl.getShaderParameter(fragmentShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Shader Program
	shaderProgramObject_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_ab, vertexShaderObject_ab);
	gl.attachShader(shaderProgramObject_ab, fragmentShaderObject_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexture");
	
	//Linking
	gl.linkProgram(shaderProgramObject_ab);
	
	if(gl.getProgramParameter(shaderProgramObject_ab, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

    //Load Pyramid Texture
	marble_texture_ab = gl.createTexture();
	marble_texture_ab.image = new Image();
	marble_texture_ab.image.crossOrigin = "";
	marble_texture_ab.image.src = "marble.png";
	marble_texture_ab.image.onload = function ()
	{
	    gl.bindTexture(gl.TEXTURE_2D, marble_texture_ab);
	    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
	    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, marble_texture_ab.image);
	    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	    gl.bindTexture(gl.TEXTURE_2D, null);
	}

	//Get MVP Uniform location
	mUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_m_matrix");
	vUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_v_matrix");
	pUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_p_matrix");
	laUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_la");
	ldUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_ld");
	lsUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_ls");
	kaUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_ka");
	kdUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_kd");
	ksUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_ks");
	lightPositionUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_light_position");
	materialShininessUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_material_shininess");
	LKeyPressedUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_lIsPressed");
	samplerUniform_ab = gl.getUniformLocation(shaderProgramObject_ab, "u_sampler");

	var cubeVCNT = new Float32Array([1.0, 1.0, -1.0, 1.0, 1.0, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0,
	                                    -1.0, 1.0, -1.0, 1.0, 1.0, 0.0, -1.0, 1.0, -1.0, 1.0, 0.0,
	                                    -1.0, 1.0, 1.0, 1.0, 1.0, 0.0, -1.0, 1.0, 0.0, 1.0, 1.0,
	                                    1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0,
	                                    1.0, -1.0, -1.0, 0.0, 1.0, 1.0, 1.0, -1.0, 1.0, 0.0, 0.0,
	                                    -1.0, -1.0, -1.0, 0.0, 1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 0.0,
	                                    -1.0, -1.0,  1.0, 0.0, 1.0, 1.0, -1.0, -1.0, -1.0, 1.0, 1.0,
	                                    1.0, -1.0,  1.0, 0.0, 1.0, 1.0, 1.0, -1.0, -1.0, 0.0, 1.0,
	                                    1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
	                                    -1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
	                                    -1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
	                                    1.0, -1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,
	                                    1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 1.0, -1.0, -1.0, 0.0, 0.0,
	                                    -1.0, 1.0, -1.0, 0.0, 0.0, 1.0, -1.0, -1.0, -1.0, 1.0, 0.0,
	                                    -1.0, -1.0, -1.0, 0.0, 0.0, 1.0, -1.0, 1.0, -1.0, 1.0, 1.0,
	                                    1.0, -1.0, -1.0, 0.0, 0.0, 1.0, 1.0, 1.0, -1.0, 0.0, 1.0,
	                                    1.0, 1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 0.0, 0.0,
	                                    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0,
	                                    1.0, -1.0, 1.0, 1.0, 1.0, 1.0, 1.0, -1.0, 1.0, 1.0, 1.0,
	                                    1.0, -1.0, -1.0, 1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 0.0, 1.0,
	                                    -1.0, 1.0, 1.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
	                                    -1.0, 1.0, -1.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 1.0, 0.0,
	                                    -1.0, -1.0, -1.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 1.0, 1.0,
	                                    -1.0, -1.0, 1.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.0, 1.0]);

	//Vao for Cube
	vao_cube_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_cube_ab);

	vbo_cube_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_ab);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVCNT, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, cubeVCNT.BYTES_PER_ELEMENT * 11, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);

	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, cubeVCNT.BYTES_PER_ELEMENT * 11, cubeVCNT.BYTES_PER_ELEMENT * 3);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, cubeVCNT.BYTES_PER_ELEMENT * 11, cubeVCNT.BYTES_PER_ELEMENT * 6);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, cubeVCNT.BYTES_PER_ELEMENT * 11, cubeVCNT.BYTES_PER_ELEMENT * 9);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	gl.bindVertexArray(null);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen == true)
	{
		canvas.width=window.innerWidth;
		canvas.height=window.innerHeight;
	}
	else
	{
		canvas.width=canvas_original_width;
		canvas.height=canvas_original_height;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
	
}

function draw()
{
	//Code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject_ab);

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
	mat4.rotateX(modelMatrix, modelMatrix, degToRad(angle_cube));
	mat4.rotateY(modelMatrix, modelMatrix, degToRad(angle_cube));
	mat4.rotateZ(modelMatrix, modelMatrix, degToRad(angle_cube));

	gl.uniformMatrix4fv(mUniform, false, modelMatrix);
	gl.uniformMatrix4fv(vUniform, false, viewMatrix);
	gl.uniformMatrix4fv(pUniform, false, perspectiveProjectionMatrix);

	if (lKeyPressed == true)
	{
	    gl.uniform1i(LKeyPressedUniform_ab, 1);
	    gl.uniform3fv(laUniform_ab, lightAmbient_ab);
	    gl.uniform3fv(ldUniform_ab, lightDiffuse_ab);
	    gl.uniform3fv(lsUniform_ab, lightSpecular_ab);
	    gl.uniform3fv(kaUniform_ab, materialAmbient_ab);
	    gl.uniform3fv(kdUniform_ab, materialDiffuse_ab);
	    gl.uniform3fv(ksUniform_ab, materialSpecular_ab);
	    gl.uniform1f(materialShininessUniform_ab, materialShininess_ab);
	    gl.uniform4fv(lightPositionUniform_ab, lightPosition_ab);
	}
	else
	{
	    gl.uniform1i(LKeyPressedUniform_ab, 0);
	}

    //Bind with Texture
	gl.bindTexture(gl.TEXTURE_2D, marble_texture_ab);
	gl.uniform1i(samplerUniform_ab, 0);

	gl.bindVertexArray(vao_cube_ab);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	
	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();

	//Animation Loop
	requestAnimationFrame(draw, canvas);
}

function update()
{
    //Code
	angle_cube = angle_cube + 1.0;

	if (angle_cube >= 360.0)
	{
		angle_cube = 0.0;
	}
}

function uninitialize()
{
	//Code
	if(vao_cube_ab)
	{
	    gl.deleteVertexArray(vao_cube_ab);
	    vao_cube_ab = null;
	}

	if (vbo_cube_ab)
	{
	    gl.deleteBuffer(vbo_cube_ab);
	    vbo_cube_ab = null;
	}

	if (marble_texture_ab)
	{
	    gl.deleteTexture(marble_texture_ab);
	    marble_texture_ab = 0;
	}

	if(shaderProgramObject_ab)
	{
		if(fragmentShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, fragmentShaderObject_ab);
			gl.deleteShader(fragmentShaderObject_ab);
			fragmentShaderObject_ab = null;
		}

		if(vertexShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, vertexShaderObject_ab);
			gl.deleteShader(vertexShaderObject_ab);
			vertexShaderObject_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_ab);
		shaderProgramObject_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{
	    case 27:
	        //Escape for uninitialize
	        uninitialize();
	        //Close our Application Tab
	        window.close();
	        break;

	    case 91: // For 'F1'
	        toggleFullscreen();
	        break;

	    case 86: // For 'V' of 'v'
	        shaderKeypress = 1;
	        break;

	    case 70: // For 'F' of 'f'
	        shaderKeypress = 2;
	        break;

	    case 76: // For 'L' or 'l'
	        if (lKeyPressed == false)
	        {
	            lKeyPressed = true;
	        }
	        else
	        {
	            lKeyPressed = false;
	        }
	        break;
	}
}

function mouseDown()
{
	//Code
}

function degToRad(degrees)
{
	return(degrees * Math.PI / 180);
}
