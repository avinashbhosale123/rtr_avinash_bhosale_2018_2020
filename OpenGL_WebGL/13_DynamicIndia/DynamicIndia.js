//Global Variables
var canvas_ab=null;
var gl=null;
var bFullscreen_ab=false;
var canvas_original_width_ab;
var canvas_original_height_ab;

const WebGLMacros = 
{
AMC_ATTRIBUTE_VERTEX:0,
AMC_ATTRIBUTE_COLOR:1,
AMC_ATTRIBUTE_NORMAL:2,
AMC_ATTRIBUTE_TEXTURE0:3
};

var vertexShaderObject_ab;
var fragmentShaderObject_ab;
var shaderProgramObject_ab;

var vao_il_ab, vao_n_ab, vao_d_ab, vao_ir_ab, vao_a_ab, vao_strips_ab, vao_plane, vao_exhaust_ab;
var vbo_il_position_ab, vbo_il_color_ab, vbo_n_position_ab, vbo_n_color_ab, vbo_d_position_ab, vbo_d_color_ab, vbo_plane_position_ab, vbo_plane_color_ab;
var vbo_ir_position_ab, vbo_ir_color_ab, vbo_a_position_ab, vbo_a_color_ab, vbo_strips_position_ab, vbo_strips_color_ab, vbo_exhaust_position_ab, vbo_exhaust_color_ab;
var mvpuniform;
var perspectiveProjectionMatrix;
var plane_x = -2.50;
var translateILx = -2.5;
var translateNy = 2.5;
var translateIRy = -2.5;
var translateAx = 3.0;
var colorSaffron1 = 0.0;
var colorSaffron2 = 0.0;
var colorSaffron3 = 0.0;
var colorGreen1 = 0.0;
var colorGreen2 = 0.0;
var colorGreen3 = 0.0;
var plane_rotation = -90.0;
var plane_radius = 0;
var i = 1000;
var exhaust_x1 = -0.75, exhaust_x2 = -0.70;
var plane_angle;
var plane_x2 = 0.0;
var counter = 0.0;

//To start Animation
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame;

//To stop Animation
var cancelAnimationFrame =
window.cancelAnimationFrame ||
window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame ||
window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame ||
window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame ||
window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame;

//Onload function
function main()
{
	//Get canvas_ab element
	canvas_ab = document.getElementById("AMC");
	if(!canvas_ab)
		console.log("Obtaining Canvas Failed\n");
	else
		console.log("Obtaining Canvas Succeeded\n");

	canvas_original_width_ab=canvas_ab.width;
	canvas_original_height_ab=canvas_ab.height;

	//Register event handlers
	window.addEventListener("keydown", keyDown, false);
	window.addEventListener("click", mouseDown, false);
	window.addEventListener("resize", resize, false);

	//Initialize WebGL
	init();

	//Start Drawing
	resize();
	draw();
}

function toggleFullscreen()
{
	//Code
	var fullscreen_element = 
	document.fullscreenElement ||
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	//If Fullscreen is not enabled
	if(fullscreen_element==null)
	{
		if(canvas_ab.requestFullscreen)
			canvas_ab.requestFullscreen();
		else if(canvas_ab.mozRequestFullScreen)
			canvas_ab.mozRequestFullScreen();
		else if (canvas_ab.webkitRequestFullscreen)
			canvas_ab.webkitRequestFullscreen();
		else if(canvas_ab.msRequestFullscreen)
			canvas_ab.msRequestFullscreen();
		bFullscreen_ab=true;
	}
	else
	{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullscreen_ab=false;
	}
}

function init()
{
    //Code
	//Get the WebGL 2.0 Context
	gl = canvas_ab.getContext("webgl2"); //webgl1 in case of mobile browser

	//If Getting Context Fails
	if(gl == null)
	{
		console.log("Failed to get the Rendering Context for WebGL");
		return;
	}
	
	gl.viewportWidth = canvas_ab.width;
	gl.viewpoetHeight = canvas_ab.height;

	//Vertex Shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
    "in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
    "out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
    "out_color = vColor;" +
	"}";
	
	vertexShaderObject_ab = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_ab, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject_ab);
	
	if(gl.getShaderParameter(vertexShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(vertexShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Fragment Shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
    "in vec4 out_color;" +
	"out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = out_color;" +
    "}";
	
	fragmentShaderObject_ab = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_ab, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject_ab);
	
	if(gl.getShaderParameter(fragmentShaderObject_ab, gl.COMPILE_STATUS) == false)
	{
		var error = gl.ShaderInfoLog(fragmentShaderObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Shader Program
	shaderProgramObject_ab = gl.createProgram();
	gl.attachShader(shaderProgramObject_ab, vertexShaderObject_ab);
	gl.attachShader(shaderProgramObject_ab, fragmentShaderObject_ab);

	//Pre-Linking of the Shader Program Object with Vertex Shader Attributes
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
	gl.bindAttribLocation(shaderProgramObject_ab, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
	
	//Linking
	gl.linkProgram(shaderProgramObject_ab);
	
	if(gl.getProgramParameter(shaderProgramObject_ab, gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_ab);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	//Get MVP Uniform location
	mvpUniform = gl.getUniformLocation(shaderProgramObject_ab, "u_mvp_matrix");

	var ilVertices = new Float32Array([-0.60, 0.60, 0.0, -0.50, 0.60, 0.0,
                                       -0.55, 0.60, 0.0, -0.55, -0.60, 0.0, 
                                       -0.60, -0.60, 0.0, -0.50, -0.60, 0.0]);

	var ilColor = new Float32Array([1.00, 0.60, 0.20, 1.00, 0.60, 0.20, 
                                    1.00, 0.60, 0.20, 0.07, 0.53, 0.027,
                                    0.07, 0.53, 0.027, 0.07, 0.53, 0.027]);

	var nVertices = new Float32Array([-0.40, 0.60, 0.00, -0.40, -0.60, 0.0, 
                                      -0.40, 0.60, 0.00, -0.20, -0.60, 0.0, 
                                      -0.20, -0.60, 0.0, -0.20, 0.60, 0.0]);

	var nColor = new Float32Array([1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   0.07, 0.53, 0.027, 1.00, 0.60, 0.20]);
    
	var dVertices = new Float32Array([-0.10, 0.60, 0.00, -0.10, -0.60, 0.0, 
                                      -0.15, 0.60, 0.00, 0.10, 0.60, 0.00, 
                                      0.10, 0.60, 0.00, 0.10, -0.60, 0.0, 
                                      0.10, -0.60, 0.0, -0.15, -0.60, 0.00]);
    
	var irVertices = new Float32Array([0.20, 0.60, 0.0, 0.30, 0.60, 0.0, 
                                       0.25, 0.60, 0.0, 0.25, -0.60, 0.0, 
                                       0.20, -0.60, 0.0, 0.30, -0.60, 0.0]);
    
	var irColor = new Float32Array([1.00, 0.60, 0.20, 1.00, 0.60, 0.20, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027, 
                                   0.07, 0.53, 0.027, 0.07, 0.53, 0.027]);
    
	var aVertices = new Float32Array([0.60, -0.60, 0.0, 0.50, 0.60, 0.0, 
                                      0.50, 0.60, 0.0, 0.40, -0.60, 0.0]);

	var aColor = new Float32Array([0.07, 0.53, 0.027, 1.00, 0.60, 0.20, 
                                   1.00, 0.60, 0.20, 0.07, 0.53, 0.027]);

	var planeVertices = new Float32Array([0.0, 0.025, 0.0, -0.05, 0.0, 0.0, 0.05, 0.0, 0.0, 
                                         0.0, -0.025, 0.0, 0.05, 0.0, 0.0, -0.05, 0.0, 0.0, 
                                         -0.05, 0.0, 0.0, -0.075, 0.025, 0.0, -0.075, -0.025, 0.0, 
                                         0.0, -0.05, 0.0, -0.025, 0.0, 0.0, 0.025, 0.0, 0.0, 
                                         0.0, 0.05, 0.0, -0.025, 0.0, 0.0, 0.025, 0.0, 0.0]);

	var planeColor = new Float32Array([0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 
                                       0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 
                                       0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 
                                       0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 
                                       0.726, 0.882, 0.929, 0.726, 0.882, 0.929, 0.726, 0.882, 0.929]);

	var exhaustColor = new Float32Array([0.0, 0.0, 0.0, 1.00, 0.60, 0.20, 
                                         0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 
                                         0.0, 0.0, 0.0, 0.07, 0.53, 0.027]);

	//Vao for Triangle
	vao_il_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_il_ab);

	vbo_il_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_il_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, ilVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_il_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_il_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, ilColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_n_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_n_ab);

	vbo_n_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_n_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, nVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_n_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_n_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, nColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_d_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_d_ab);

	vbo_d_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_d_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, dVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_ir_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_ir_ab);

	vbo_ir_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_ir_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, irVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_ir_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_ir_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, irColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_a_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_a_ab);

	vbo_a_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_a_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, aVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_a_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_a_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, aColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_plane = gl.createVertexArray();
	gl.bindVertexArray(vao_plane);

	vbo_plane_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_plane_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, planeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_plane_position_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_plane_position_ab);
	gl.bufferData(gl.ARRAY_BUFFER, planeColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	vao_exhaust_ab = gl.createVertexArray();
	gl.bindVertexArray(vao_exhaust_ab);

	vbo_exhaust_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_exhaust_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, exhaustColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);
	
	//Set the Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0);

	//Initialize Projection Matrix
	perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
	//Code
	if(bFullscreen_ab == true)
	{
		canvas_ab.width=window.innerWidth;
		canvas_ab.height=window.innerHeight;
	}
	else
	{
		canvas_ab.width=canvas_original_width_ab;
		canvas_ab.height=canvas_original_height_ab;
	}

	//Set the viewport
	gl.viewport(0, 0, canvas_ab.width, canvas_ab.height);

	//Perspective Projection
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas_ab.width)/parseFloat(canvas_ab.height), 0.1, 100.0);
	
}

function draw()
{
    //Code
	gl.clear(gl.COLOR_BUFFER_BIT);
	
	gl.useProgram(shaderProgramObject_ab);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [translateILx, 0.0, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw Left I
	gl.bindVertexArray(vao_il_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, translateNy, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw N
	gl.bindVertexArray(vao_n_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw D
	gl.bindVertexArray(vao_d_ab);

	var dColor = [];

	dColor.push(colorSaffron1);
	dColor.push(colorSaffron2);
	dColor.push(colorSaffron3);
	dColor.push(colorGreen1);
	dColor.push(colorGreen2);
	dColor.push(colorGreen3);
	dColor.push(colorSaffron1);
	dColor.push(colorSaffron2);
	dColor.push(colorSaffron3);
	dColor.push(colorSaffron1);
	dColor.push(colorSaffron2);
	dColor.push(colorSaffron3);
	dColor.push(colorSaffron1);
	dColor.push(colorSaffron2);
	dColor.push(colorSaffron3);
	dColor.push(colorGreen1);
	dColor.push(colorGreen2);
	dColor.push(colorGreen3);
	dColor.push(colorGreen1);
	dColor.push(colorGreen2);
	dColor.push(colorGreen3);
	dColor.push(colorGreen1);
	dColor.push(colorGreen2);
	dColor.push(colorGreen3);

	vbo_d_color_ab = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_d_color_ab);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(dColor), gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

	gl.drawArrays(gl.LINES, 0, 8);

	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, translateIRy, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw Right I
	gl.bindVertexArray(vao_ir_ab);

	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindVertexArray(null);

	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [translateAx, 0.0, -3.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw A
	gl.bindVertexArray(vao_a_ab);

	gl.drawArrays(gl.LINES, 0, 4);

	gl.bindVertexArray(null);

    //Translate for Horizontal Plane
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [plane_x, 0.0, -2.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw the Plane
	gl.bindVertexArray(vao_plane);

	gl.drawArrays(gl.TRIANGLES, 0, 15);

	gl.bindVertexArray(null);

    //Translate for Other Planes
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.6, 1.2, 0.0]);
	mat4.translate(modelViewMatrix, modelViewMatrix, [parseFloat((-Math.cos(plane_angle)) + plane_x2), parseFloat(-Math.sin(plane_angle)), -2.0]);
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(plane_rotation));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw the Plane
	gl.bindVertexArray(vao_plane);

	gl.drawArrays(gl.TRIANGLES, 0, 15);

	gl.bindVertexArray(null);

    //Translate for Other Planes
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.6, -1.2, 0.0]);
	mat4.translate(modelViewMatrix, modelViewMatrix, [parseFloat((-Math.cos(plane_angle) + plane_x2)), parseFloat(Math.sin(plane_angle)), -2.0]);
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(-plane_rotation));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw the Plane
	gl.bindVertexArray(vao_plane);

	gl.drawArrays(gl.TRIANGLES, 0, 15);

	gl.bindVertexArray(null);

	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -2.0]);;
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    //Draw the Exhaust
	gl.bindVertexArray(vao_exhaust_ab);

	if (plane_x >= -0.60)
	{
	    var exhaust = [];

	    exhaust.push(exhaust_x1);
	    exhaust.push(0.01);
	    exhaust.push(0.0);

	    exhaust.push(exhaust_x2);
	    exhaust.push(0.01);
	    exhaust.push(0.0)

	    exhaust.push(exhaust_x1);
	    exhaust.push(0.0);
	    exhaust.push(0.0)

	    exhaust.push(exhaust_x2);
	    exhaust.push(0.0);
	    exhaust.push(0.0)

	    exhaust.push(exhaust_x1);
	    exhaust.push(-0.01);
	    exhaust.push(0.0);

	    exhaust.push(exhaust_x2);
	    exhaust.push(-0.01);
	    exhaust.push(0.0);

	    vbo_exhaust_position_ab = gl.createBuffer();
	    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_exhaust_position_ab);
	    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(exhaust), gl.DYNAMIC_DRAW);
	    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
	    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	}
    
	gl.drawArrays(gl.LINES, 0, 6);

	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();

	if (vbo_d_color_ab)
	{
	    gl.deleteBuffer(vbo_d_color_ab);
	    vbo_d_color_ab = null;
	}

	if (vbo_exhaust_position_ab)
	{
	    gl.deleteBuffer(vbo_exhaust_position_ab);
	    vbo_exhaust_position_ab = null;
	}

	//Animation Loop
	requestAnimationFrame(draw, canvas_ab);
}

function update()
{
    //Code
    //Condition to translate I coming in rom Let side
    if(translateILx <= 0.0)
    {
        translateILx = translateILx + 0.005;
    }

    //Condition to translate A coming in rom Right side
    if(translateILx >= 0.0)
    {
        if(translateAx >= 0.0)
        {
            translateAx = translateAx - 0.005;
        }
    }

    //Condition to translate N coming in rom Top side
    if(translateAx <= 0.0)
    {
        if(translateNy >= 0.0)
        {
            translateNy = translateNy - 0.005;
        }
    }

    //Condition to translate I coming in rom Bottom side
    if(translateNy <= 0.0)
    {
        if(translateIRy <= 0.0)
        {
            translateIRy = translateIRy + 0.005;
        }

        if(translateIRy >= 0.0)
        {
            translateIRy = 0.0;
        }
    }

    //Condition to Fade In D using Color variables
    if(translateIRy >= 0.0)
    {
        colorSaffron1 = colorSaffron1 + 0.002;
        
        if(colorSaffron1 >= 1.0)
        {
            colorSaffron1 = 1.0;
        }
  
        colorSaffron2 = colorSaffron2 + 0.001;

        if(colorSaffron2 >= 0.60)
        {
            colorSaffron2 = 0.60;
        }
        
        colorSaffron3 = colorSaffron3 + 0.003;

        if(colorSaffron3 >= 0.20)
        {
            colorSaffron3 = 0.20;
        }

        colorGreen1 = colorGreen1 + 0.010;

        if(colorGreen1 >= 0.07)
        {
            colorGreen1 = 0.07;
        }

        colorGreen2 = colorGreen2 + 0.002;

        if(colorGreen2 >= 0.53)
        {
            colorGreen2 = 0.53;
        }

        colorGreen3 = colorGreen3 + 0.010;

        if(colorGreen3 >= 0.027)
        {
            colorGreen3 = 0.027;
        }        
    }

    //Condition to translate the plane coming in Horizontally
    if (colorSaffron1 >= 1.0) 
    {
        if (plane_x <= 0.65) 
        {
            plane_x = plane_x + 0.00361;
        }

        if (plane_radius <= i) 
        {
            plane_radius = plane_radius + 2;

            plane_angle = (Math.PI / 2) * plane_radius / i;

            if (plane_rotation <= 0.0) 
            {
                plane_rotation = plane_rotation + 0.2;
            }
        }

        //Condition to translate the Planes coming in from an angle to move Horizontally ater the angle ends on the x-axis
        if (plane_radius >= i)
        {
            if (plane_x >= 0.65)
            {
                plane_x2 = plane_x2 + 0.00361;
            }
        }

        if (counter <= 1000)
        {
            counter = counter + 1;
        }

        if (counter >= 1000)
        {

            if (plane_x <= 2.5)
            {
                plane_x = plane_x + 0.00361;
            }

            if (plane_radius <= 2 * i)
            {
                plane_radius = plane_radius + 2;

                plane_angle = (Math.PI / 2) * plane_radius / i;
            }

            plane_rotation = plane_rotation + 0.2;

        }
    }


    //Condition to start drawing the exhaust o Tri Color
    if(plane_x >= -0.60)
    {
        if(exhaust_x2 <= 0.36)
        {
            exhaust_x2 = exhaust_x2 + 0.00361;
        }
    }

    //Condition to Fade out the Tri Color to a certain point on A
    if (exhaust_x2 >= 0.36)
    {
        if (exhaust_x1 <= 0.27)
        {
            exhaust_x1 = exhaust_x1 + 0.00361;
        }
    }
}

function uninitialize()
{
	//Code
	if (vbo_il_position_ab)
	{
	    gl.deleteBuffer(vbo_il_position_ab);
	    vbo_il_position_ab = null;
	}

	if (vbo_il_color_ab)
	{
	    gl.deleteBuffer(vbo_il_color_ab);
	    vbo_il_color_ab = null;
	}

	if (vbo_n_position_ab)
	{
	    gl.deleteBuffer(vbo_n_position_ab);
	    vbo_n_position_ab = null;
	}

	if (vbo_n_color_ab)
	{
	    gl.deleteBuffer(vbo_n_color_ab);
	    vbo_n_color_ab = null;
	}

	if (vbo_d_position_ab)
	{
	    gl.deleteBuffer(vbo_d_position_ab);
	    vbo_d_position_ab = null;
	}

	if (vbo_a_position_ab)
	{
	    gl.deleteBuffer(vbo_a_position_ab);
	    vbo_a_position_ab = null;
	}

	if (vbo_a_color_ab)
	{
	    gl.deleteBuffer(vbo_a_color_ab);
	    vbo_a_color_ab = null;
	}

	if (vbo_ir_position_ab)
	{
	    glDeleteBuffer(vbo_ir_position_ab);
	    vbo_ir_position_ab = 0;
	}

	if (vbo_ir_color_ab)
	{
	    glDeleteBuffer(vbo_ir_color_ab);
	    vbo_ir_color_ab = 0;
	}

	if (vbo_exhaust_color_ab)
	{
	    glDeleteBuffer(vbo_exhaust_color_ab);
	    vbo_exhaust_color_ab = 0;
	}

	if (vao_il_ab)
	{
	    gl.deleteVertexArray(vao_il_ab);
	    vao_il_ab = null;
	}

	if (vao_n_ab)
	{
	    gl.deleteVertexArray(vao_n_ab);
	    vao_n_ab = null;
	}

	if (vao_d_ab)
	{
	    gl.deleteVertexArray(vao_d_ab);
	    vao_d_ab = null;
	}

	if (vao_ir_ab)
	{
	    gl.deleteVertexArray(vao_ir_ab);
	    vao_ir_ab = null;
	}

	if (vao_a_ab)
	{
	    gl.deleteVertexArray(vao_a_ab);
	    vao_a_ab = null;
	}

	if (vao_exhaust_ab)
	{
	    gl.deleteVertexArray(vao_exhaust_ab);
	    vao_exhaust_ab = null;
	}

	if(shaderProgramObject_ab)
	{
		if(fragmentShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, fragmentShaderObject_ab);
			gl.deleteShader(fragmentShaderObject_ab);
			fragmentShaderObject_ab = null;
		}

		if(vertexShaderObject_ab)
		{
			gl.detachShader(shaderProgramObject_ab, vertexShaderObject_ab);
			gl.deleteShader(vertexShaderObject_ab);
			vertexShaderObject_ab = null;
		}

		gl.deleteProgram(shaderProgramObject_ab);
		shaderProgramObject_ab = null;
	}
}

function keyDown(event)
{
	//Code
	switch(event.keyCode)
	{

		case 27:
			//Escape for uninitialize
			uninitialize();
			//Close our Application Tab
			window.close();
			break;

		case 70: // For 'F' or 'f'
			toggleFullscreen();
			break;
	}
}

function mouseDown()
{
	//Code
}

function degToRad(degrees)
{
    return (degrees * Math.PI / 180);
}