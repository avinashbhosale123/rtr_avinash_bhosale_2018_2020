#include<stdio.h>
#include<stdlib.h>

int main(void)
{
	//Pointer Declaration
	int **ptr = NULL;
	int i, j;

	//Code
	ptr = (int **)malloc(5 * sizeof(int *));
	if (ptr == NULL)
	{
		printf("Memory Allocation Failed\n");
		exit(0);
	}

	for (i = 0; i < 5; i++)
	{
		ptr[i] = (int *)malloc(3 * sizeof(int));

		if (ptr[i] == NULL)
		{
			printf("Memory Allocation Failed\n");
			exit(0);
		}
	}

	printf("Enter the Values for the Array Elements:\n");
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 3; j++)
		{
			scanf("%d", (ptr[i]+j));
		}
	}

	printf("\nPrinting the Elements of the Array and their Addresses:\n");
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 3; j++)
		{
			printf("Value: %d, Address: %p\n", *(ptr[i]+j), (ptr[i]+j));
		}
	}

	printf("\nPrinting the Elements PTR and their Addresses:\n");
	for (i = 0; i < 5; i++)
	{
		printf("%d Element Address: %p, value: %p\n", i + 1, ptr + i, *(ptr+i));
	}

	//Free Memory of PTR
	for (i = 0; i < 5; i++)
	{
		if (ptr[i])
		{
			free(ptr[i]);
			ptr[i] = NULL;
		}
	}

	if (ptr)
	{
		free(ptr);
		ptr = NULL;
	}

	return 0;
}