#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Variable Declaration
	struct MyData data;

	//Code
	data.i = 30;
	data.f = 11.45f;
	data.d = 1.2995;
	data.c = 'A';

	printf("\nDATA MEMBERS of the 'struct MyData': \n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n", data.c);

	printf("\nADDRESSES of the DATA MEMBERS of 'struct MyData': \n");
	printf("'i' Occupies Addresses From %p\n", &data.i);
	printf("'f' Occupies Addresses From %p\n", &data.f);
	printf("'d' Occupies Addresses From %p\n", &data.d);
	printf("'c' Occupies Address %p\n", &data.c);

	printf("\nStarting Address of the 'struct MyData' variable 'data' = %p\n", &data);

	return 0;
}

