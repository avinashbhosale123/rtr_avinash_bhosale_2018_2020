#include <stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int x;
		int y;
	} point_01, point_02;

} rect = { {1, 3}, {3, 6} };

int main(void)
{
	//Variable Declarations
	int length, breadth, area;

	//Code
	length = rect.point_02.y - rect.point_01.y;

	if (length < 0)
	{
		length = length * -1;
	}

	breadth = rect.point_02.x - rect.point_01.x;
	
	if (breadth < 0)
	{
		breadth = breadth * -1;
	}

	area = length * breadth;

	printf("\nLength of the Rectangle = %d\n", length);
	printf("\nBreadth of the Rectangle = %d\n", breadth);
	printf("\nArea of the Rectangle = %d\n", area);

	return 0;
}
