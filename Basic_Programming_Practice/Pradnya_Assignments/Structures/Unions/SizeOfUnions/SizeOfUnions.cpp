#include <stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Variable Declarations
	struct MyStruct s;
	union MyUnion u;

	//Code
	printf("\nSize of MyStruct = %d\n", sizeof(s));

	printf("\nSize of MyUnion = %d\n", sizeof(u));

	return 0;
}

