#include <stdio.h>
#include <conio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Function Declaration
	struct MyData AddStructMembers(struct MyData, struct MyData, struct MyData);

	//Variable Declarations
	struct MyData data1, data2, data3, answer_data;

	//Code
	//Data 1
	printf("\n****** DATA 1 ******\n");
	printf("\nEnter an Integer Value for 'i' of the 'struct MyData data1': \n");
	scanf("%d", &data1.i);

	printf("\nEnter a Floating Point Value for 'f' of the 'struct MyData data1': \n");
	scanf("%f", &data1.f);

	printf("\nEnter a Double Value for 'd' of the 'struct MyData data1': \n");
	scanf("%lf", &data1.d);

	printf("\nEnter a Character for 'c' of the 'struct MyData data1': \n");
	data1.c = getch();
	printf("%c", data1.c);

	//Data 2
	printf("\n****** DATA 2 ******\n");
	printf("\nEnter an Integer Value for 'i' of the 'struct MyData data2': \n");
	scanf("%d", &data2.i);

	printf("\nEnter a Floating Point Value for 'f' of the 'struct MyData data2': \n");
	scanf("%f", &data2.f);

	printf("\nEnter a Double Value for 'd' of the 'struct MyData data2': \n");
	scanf("%lf", &data2.d);

	printf("\nEnter a Character for 'c' of the 'struct MyData data2': \n");
	data1.c = getch();
	printf("%c", data2.c);

	//Data 3
	printf("\n****** DATA 3 ******\n");
	printf("\nEnter an Integer Value for 'i' of the 'struct MyData data3': \n");
	scanf("%d", &data3.i);

	printf("\nEnter a Floating Point Value for 'f' of the 'struct MyData data3': \n");
	scanf("%f", &data3.f);

	printf("\nEnter a Double Value for 'd' of the 'struct MyData data3': \n");
	scanf("%lf", &data3.d);

	printf("\nEnter a Character for 'c' of the 'struct MyData data3': \n");
	data1.c = getch();
	printf("%c", data3.c);

	answer_data = AddStructMembers(data1, data2, data3);

	printf("\n****** ANSWER ******\n");
	printf("answer_data.i = %d\n", answer_data.i);
	printf("answer_data.f = %f\n", answer_data.f);
	printf("answer_data.d = %lf\n", answer_data.d);

	answer_data.c = data1.c;
	printf("\nanswer_data.c (from data1) = %c\n", answer_data.c);

	answer_data.c = data2.c;
	printf("\nanswer_data.c (from data2) = %c\n", answer_data.c);

	answer_data.c = data3.c;
	printf("\nanswer_data.c (from data3) = %c\n", answer_data.c);

	return 0;
}

struct MyData AddStructMembers(struct MyData md_one, struct MyData md_two, struct MyData md_three)
{
	//Variable Declaration
	struct MyData answer;

	//Code
	answer.i = md_one.i + md_two.i + md_three.i;
	answer.f = md_one.f + md_two.f + md_three.f;
	answer.d = md_one.d + md_two.d + md_three.d;

	return(answer);
}
