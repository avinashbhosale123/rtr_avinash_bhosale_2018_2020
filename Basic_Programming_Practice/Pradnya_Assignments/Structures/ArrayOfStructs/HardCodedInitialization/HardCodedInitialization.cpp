#include <stdio.h>
#include<string.h>

//Macros
#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char marital_status[MARITAL_STATUS];
};

int main(void)
{
	//Variable Declaration
	struct Employee EmployeeRecord[5];
	char employee_avinash[] = "Avinash";
	char employee_vikas[] = "Vikas";
	char employee_laxman[] = "Laxman";
	char employee_harshal[] = "Harshal";
	char employee_amit[] = "Amit";
	int i;

	//Code
	strcpy(EmployeeRecord[0].name, employee_avinash);
	EmployeeRecord[0].age = 35;
	EmployeeRecord[0].sex = 'M';
	EmployeeRecord[0].salary = 50000.0f;
	strcpy(EmployeeRecord[0].marital_status, "Unmarried");

	strcpy(EmployeeRecord[1].name, employee_vikas);
	EmployeeRecord[1].age = 31;
	EmployeeRecord[1].sex = 'M';
	EmployeeRecord[1].salary = 60000.0f;
	strcpy(EmployeeRecord[1].marital_status, "Unmarried");

	strcpy(EmployeeRecord[2].name, employee_laxman);
	EmployeeRecord[2].age = 30;
	EmployeeRecord[2].sex = 'M';
	EmployeeRecord[2].salary = 62000.0f;
	strcpy(EmployeeRecord[2].marital_status, "Unmarried");

	strcpy(EmployeeRecord[3].name, employee_harshal);
	EmployeeRecord[3].age = 37;
	EmployeeRecord[3].sex = 'M';
	EmployeeRecord[3].salary = 70000.0f;
	strcpy(EmployeeRecord[3].marital_status, "Married");

	strcpy(EmployeeRecord[4].name, employee_amit);
	EmployeeRecord[4].age = 30;
	EmployeeRecord[4].sex = 'M';
	EmployeeRecord[4].salary = 40000.0f;
	strcpy(EmployeeRecord[4].marital_status, "Married");

	printf("\n******DISPLAYING EMPLOYEE RECORDS******\n");
	for (i = 0; i < 5; i++)
	{
		printf("******EMPLOYEE NUMBER %d******\n", (i + 1));
		printf("Name: %s\n", EmployeeRecord[i].name);
		printf("Age: %d years\n", EmployeeRecord[i].age);

		if (EmployeeRecord[i].sex == 'M' || EmployeeRecord[i].sex == 'm')
		{
			printf("Sex: Male\n");
		}
		else
		{
			printf("Sex: Female\n");
		}

		printf("Salary: Rs. %f\n", EmployeeRecord[i].salary);
		printf("Marital Status: %s\n", EmployeeRecord[i].marital_status);
	}

	return 0;
}
