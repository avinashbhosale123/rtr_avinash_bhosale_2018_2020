#include <stdio.h>
#include <conio.h>
#include <ctype.h>
#include <string.h>

//Macros
#define NUM_EMPLOYEES 5
#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	char sex;
	float salary;
	char marital_status;
};

int main(void)
{
	//Function Declaration
	void MyGetString(char[], int);

	//Variable Declarations
	struct Employee EmployeeRecord[NUM_EMPLOYEES];
	int i;

	//Code
	for (i = 0; i < NUM_EMPLOYEES; i++)
	{
		printf("\n********DATA ENTRY FOR EMPLOYEE NUMBER %d********\n", (i + 1));

		printf("\nEnter the Employee Name: \n");
		MyGetString(EmployeeRecord[i].name, NAME_LENGTH);

		printf("\nEnter the Employee's Age(in years): \n");
		scanf("%d", &EmployeeRecord[i].age);

		printf("\nEnter Employee's Sex('M'/'m' for Male, 'F'/'f' for Female): \n");
		EmployeeRecord[i].sex = getch();
		printf("%c", EmployeeRecord[i].sex);
		EmployeeRecord[i].sex = toupper(EmployeeRecord[i].sex);

		printf("\nEnter the Employee's Salary(in Indian Rupees): ");
		scanf("%f", &EmployeeRecord[i].salary);

		printf("\nIs the Employee Married? ('Y'/'y' for Yes, 'N'/'n' for No): \n");
		EmployeeRecord[i].marital_status = getch();
		printf("%c", EmployeeRecord[i].marital_status);
		EmployeeRecord[i].marital_status = toupper(EmployeeRecord[i].marital_status);
	}

	printf("\n*******DISPLAYING EMPLOYEE RECORDS*******\n");
	for (i = 0; i < NUM_EMPLOYEES; i++)
	{
		printf("\n********EMPLOYEE NUMBER %d********\n", (i + 1));
		printf("Name: %s\n", EmployeeRecord[i].name);
		printf("Age: %d years\n", EmployeeRecord[i].age);

		if (EmployeeRecord[i].sex == 'M' || EmployeeRecord[i].sex == 'm')
		{
			printf("Sex: Male\n");
		}
		else if (EmployeeRecord[i].sex == 'F' || EmployeeRecord[i].sex == 'f')
		{
			printf("Sex: Female\n");
		}
		else
		{
			printf("Sex: Invalid Data Entered\n");
		}

		printf("Salary: Rs-%f\n", EmployeeRecord[i].salary);

		if (EmployeeRecord[i].marital_status == 'Y' || EmployeeRecord[i].marital_status == 'y')
		{
			printf("Marital Status: Married\n");
		}
		else if (EmployeeRecord[i].marital_status == 'N' || EmployeeRecord[i].marital_status == 'n')
		{
			printf("Marital Status: Unmarried\n");
		}
		else
		{
			printf("Marital Status: Invalid Data Entered\n");
		}

		printf("\n");
	}

	return 0;
}

void MyGetString(char str[], int str_size)
{
	//Variable Declarations
	int i;
	char ch = '\0';

	//Code
	i = 0;
	do
	{
		ch = getch();
		str[i] = ch;
		printf("%c", str[i]);
		i++;
	} while ((ch != '\r') && (i < str_size));

	if (i == str_size)
	{
		str[i - 1] = '\0';
	}
	else
	{
		str[i] = '\0';
	}
}

