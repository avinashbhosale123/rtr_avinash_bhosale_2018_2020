#include <stdio.h>

struct MyPoint
{
	int x;
	int y;
};

int main(void)
{
	//Variable Declarations
	struct MyPoint point_A, point_B, point_C, point_D, point_E;

	//Code
	printf("Enter the x-coordinate for Point 'A': \n");
	scanf("%d", &point_A.x);
	printf("Enter the y-coordinate for Point 'A': \n");
	scanf("%d", &point_A.y);

	printf("Enter the x-coordinate for Point 'B': \n");
	scanf("%d", &point_B.x);
	printf("Enter the y-coordinate for Point 'B': \n");
	scanf("%d", &point_B.y);

	printf("\nEnter the x-coordinate for Point 'C': \n");
	scanf("%d", &point_C.x);
	printf("Enter the y-coordinate for Point 'C': \n");
	scanf("%d", &point_C.y);

	printf("\nEnter the x-coordinate for Point 'D': \n");
	scanf("%d", &point_D.x);
	printf("Enter the y-coordinate for Point 'D': \n");
	scanf("%d", &point_D.y);

	printf("\nEnter the x-coordinate for Point 'E': \n");
	scanf("%d", &point_E.x);
	printf("Enter the y-coordinate for Point 'E': \n");
	scanf("%d", &point_E.y);

	printf("\nCo-ordinates(x, y) of Point 'A': (%d, %d)\n", point_A.x, point_A.y);
	printf("\nCo-ordinates(x, y) of Point 'B': (%d, %d)\n", point_B.x, point_B.y);
	printf("\nCo-ordinates(x, y) of Point 'C': (%d, %d)\n", point_C.x, point_C.y);
	printf("\nCo-ordinates(x, y) of Point 'D': (%d, %d)\n", point_D.x, point_D.y);
	printf("\nCo-ordinates(x, y) of Point 'E': (%d, %d)\n", point_E.x, point_E.y);

	return 0;
}
