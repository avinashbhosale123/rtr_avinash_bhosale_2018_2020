#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Variable Declarations
	struct MyData data_one = { 35, 3.9f, 1.23765, 'A' };
	struct MyData data_two = { 'P', 6.2f, 12.199523, 68 };
	struct MyData data_three = { 36, 'G' };
	struct MyData data_four = { 79 };

	//Code
	printf("\nData Members of 'struct MyData data_one': \n");
	printf("i = %d\n", data_one.i);
	printf("f = %f\n", data_one.f);
	printf("d = %lf\n", data_one.d);
	printf("c = %c\n", data_one.c);

	printf("\nData Members of 'struct MyData data_two': \n");
	printf("i = %d\n", data_two.i);
	printf("f = %f\n", data_two.f);
	printf("d = %lf\n", data_two.d);
	printf("c = %c\n", data_two.c);

	printf("\nData Members of 'struct MyData data_three': \n");
	printf("i = %d\n", data_three.i);
	printf("f = %f\n", data_three.f);
	printf("d = %lf\n", data_three.d);
	printf("c = %c\n", data_three.c);

	printf("\nData Members of 'struct MyData data_four': \n");
	printf("i = %d\n", data_four.i);
	printf("f = %f\n", data_four.f);
	printf("d = %lf\n", data_four.d);
	printf("c = %c\n", data_four.c);

	return 0;
}

