#include <stdio.h>
#include <string.h>

int main(void)
{
	//Local Structure Declarations
	struct MyPoint
	{
		int x;
		int y;
	}point;

	struct MyPointProperties
	{
		int quadrant;
		char axis_location[10];
	}point_properties;

	//Code
	printf("\nEnter X-Coordinate for A Point: \n");
	scanf("%d", &point.x);
	printf("\nEnter Y-Coordinate for A Point : \n");
	scanf("%d", &point.y);

	printf("\nPoint Co-ordinates (x, y) are: (%d, %d) !\n", point.x, point.y);

	if (point.x == 0 && point.y == 0)
	{
		printf("The Point Is The Origin (%d, %d) !!!\n", point.x, point.y);
	}
	else
	{
		if (point.x == 0)
		{
			if (point.y < 0)
			{
				strcpy(point_properties.axis_location, "Negative Y");
			}

			if (point.y > 0)
			{
				strcpy(point_properties.axis_location, "Positive Y");
			}

			point_properties.quadrant = 0;

			printf("\nThe Point lies on the %s Axis !\n", point_properties.axis_location);

		}
		else if (point.y == 0)
		{
			if (point.x < 0)
			{
				strcpy(point_properties.axis_location, "Negative X");
			}

			if (point.x > 0)
			{
				strcpy(point_properties.axis_location, "Positive X");
			}

			point_properties.quadrant = 0;
			printf("\nThe Point lies on the %s Axis !\n", point_properties.axis_location);
		}
		else
		{
			point_properties.axis_location[0] = '\0';

			if (point.x > 0 && point.y > 0)
			{
				point_properties.quadrant = 1;
			}
			else if (point.x < 0 && point.y > 0)
			{
				point_properties.quadrant = 2;
			}
			else if (point.x < 0 && point.y < 0)
			{
				point_properties.quadrant = 3;
			}
			else
			{
				point_properties.quadrant = 4;
			}

			printf("\nThe Point lies in Quadrant Number %d !\n", point_properties.quadrant);
		}
	}

	return 0;
}
