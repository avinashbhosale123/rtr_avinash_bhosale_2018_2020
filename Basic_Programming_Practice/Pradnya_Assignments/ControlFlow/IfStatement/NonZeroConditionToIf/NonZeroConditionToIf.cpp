#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int a;

	//Code
	a = 5;
	if (a)
	{
		printf("\nIf Block 1: 'A' exists and has value = %d.\n", a);
	}

	a = -5;
	if (a)
	{
		printf("\nIf Block 2: 'A' exists and has value = %d.\n", a);
	}

	a = 0;
	if (a)
	{
		printf("\nIf Block 3: 'A' exists and has value = %d.\n", a);
	}

	printf("\nAll 3 if statements are done.\n");

	return 0;
}