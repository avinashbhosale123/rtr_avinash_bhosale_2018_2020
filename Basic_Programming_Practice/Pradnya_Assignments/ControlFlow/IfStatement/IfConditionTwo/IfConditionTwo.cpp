#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int age;

	//Code
	printf("\nEnter Age: ");
	scanf("%d", &age);

	if (age >= 18)
	{
		printf("\nYou are Eligible for Voting!\n");
	}

	return 0;
}