#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Even Numbers from 0 to 100 - ");

	for (i = 0; i <= 100; i++)
	{
		if (i % 2 != 0)
		{
			continue;
		}
		else
		{
			printf("\n%d", i);
		}
	}

	printf("\n");

	return 0;
}