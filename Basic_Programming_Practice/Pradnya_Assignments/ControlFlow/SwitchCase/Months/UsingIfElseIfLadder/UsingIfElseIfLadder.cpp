#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int month;

	//Code
	printf("\nEnter a Month Number (1 - 12): ");
	scanf("%d", &month);

	//Using if elseif ladder to determine the month
	if (month == 1)
		printf("\nMonth Number %d is JANUARY.\n", month);
	else if (month == 2)
		printf("\nMonth Number %d is FEBRUARY.\n", month);
	else if (month == 3)
		printf("\nMonth Number %d is MARCH.\n", month);
	else if (month == 4)
		printf("\nMonth Number %d is APRIL.\n", month);
	else if (month == 5)
		printf("\nMonth Number %d is MAY.\n", month);
	else if (month == 6)
		printf("\nMonth Number %d is JUNE.\n", month);
	else if (month == 7)
		printf("\nMonth Number %d is JULY.\n", month);
	else if (month == 8)
		printf("\nMonth Number %d is AUGUST.\n", month);
	else if (month == 9)
		printf("\nMonth Number %d is SEPTEMBER.\n", month);
	else if (month == 10)
		printf("\nMonth Number %d is OCTOBER.\n", month);
	else if (month == 11)
		printf("\nMonth Number %d is NOVEMBER.\n", month);
	else if (month == 12)
		printf("\nMonth Number %d is DECEMBER.\n", month);
	else
		printf("\nInvalid Number %d is entered!!\n", month);

	printf("\n'If-Else IF' ladder is complete.\n");

	return 0;
}