#include<stdio.h>
#include<conio.h>

int main(void)
{
	//Variable Declaration
	int a, b, result;
	char option, option_division;

	//Code
	printf("\nEnter a Value for 'A': ");
	scanf("%d", &a);

	printf("\nEnter a Value for 'B': ");
	scanf("%d", &b);

	printf("\nEnter an option in the form of a Character:");
	printf("\n'A' or 'a' for addition.");
	printf("\n'S' or 's' for subtraction.");
	printf("\n'M' or 'm' for multiplication.");
	printf("\n'D' or 'd' for division");
	printf("\nEnter an option from the above: ");
	option = getch();

	printf("\n");

	switch (option)
	{
	case 'A':
	case 'a':
		result = a + b;
		printf("\nAddition of A = %d and B = %d gives the result %d.\n", a, b, result);
		break;

	case 'S':
	case 's':
		result = a - b;
		printf("\nSubtraction of A = %d and B = %d gives the result %d.\n", a, b, result);
		break;

	case 'M':
	case 'm':
		result = a * b;
		printf("\nMultiplication of A = %d and B = %d gives the result %d.\n", a, b, result);
		break;

	case 'D':
	case 'd':
		printf("\nEnter an Option in the form of a Character:");
		printf("\n'Q' or 'q' or '/' for Quotient upon Division.");
		printf("\n'R' or 'r' or '%%' For remainder upon Division.");
		
		printf("\nEnter an Option: ");
		option_division = getch();
			   
		switch (option_division)
		{
		case 'Q':
		case 'q':
		case '/':
			if (a >= b)
			{
				result = a / b;
				printf("\nDivision Of A = %d by B = %d gives Quotient %d.\n", a, b, result);
			}
			else
			{
				result = b / a;
				printf("\nDivision Of B = %d by A = %d gives Quotient %d.\n", b, a, result);
			}
			break;

		case 'R':
		case 'r':
		case '%':
			if (a >= b)
			{
				result = a % b;
				printf("\nDivision Of A = %d By B = %d gives Remainder %d.\n", a, b, result);
			}
			else
			{
				result = b % a;
				printf("\nDivision Of B = %d By A = %d gives remainder = %d.\n", b, a, result);
			}
			break;

		default:
			printf("\nInvalid Character %c Entered!!\n", option_division);
			break;
		}
		break;

	default:
		printf("\nInvalid Character %c Entered!!\n", option);
		break;
	}

	printf("\nSwitch Block is Complete.\n");

	return 0;
}