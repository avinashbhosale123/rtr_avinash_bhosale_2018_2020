#include<stdio.h>
#include<conio.h>

//Global Constant Declarations for ASCII Values
#define CHAR_UPPERCASE_BEGINNING 65
#define CHAR_UPPERCASE_ENDING 90
#define CHAR_LOWERCASE_BEGINNING 97
#define CHAR_LOWERCASE_ENDING 122

//Global Constant Declarations for ASCII of 0 - 9
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main(void)
{
	//Variable Declaration
	char ch;
	int ch_value;

	//Code
	printf("\nEnter a Character: ");
	ch = getch();
	printf("\n");

	switch (ch)
	{
	case 'A':
	case 'a':

	case 'E':
	case 'e':

	case 'I':
	case 'i':

	case 'O':
	case 'o':

	case 'U':
	case 'u':
		printf("\nCharacter %c is a VOWEL!!\n", ch);
		break;

	default:
		ch_value = (int)ch;

		if ((ch_value >= CHAR_UPPERCASE_BEGINNING && ch_value <= CHAR_UPPERCASE_ENDING) || (ch_value >= CHAR_LOWERCASE_BEGINNING && ch_value <= CHAR_LOWERCASE_ENDING))
		{
			printf("\nCharacter %c is a CONSTANT CHARACTER!!\n", ch);
		}

		else if (ch_value >= CHAR_DIGIT_BEGINNING && ch_value <= CHAR_DIGIT_ENDING)
		{
			printf("\nCharacter %c is a DIGIT CHARACTER!!\n", ch);
		}
		else
		{
			printf("\nCharacter %c is a SPECIAL CHARACTER!!\n", ch);
		}
		break;
	}

	printf("\nSwitch Block is Complete.\n");

	return 0;
}