#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\n");

	i = 0;
	while (i <= 10)
	{
		printf("i = %d\n", i);
		printf("---------\n");
		j = 1;
		while (j <= 5)
		{
			printf("\tj = %d\n", j);
			j++;
		}
		i++;
		printf("\n");
	}

	printf("Both loops end here\n");

	return 0;
}