#include<stdio.h>

int main(void)
{
	//Variable Declaration
	float f, fnum = 1.7f;

	//Code
	printf("\nPrinting Numbers %f to %f - ", fnum, (fnum * 10.0f));

	f = fnum;
	while (f <= (fnum * 10.0f))
	{
		printf("\n%f", f);
		f = f + fnum;
	}

	printf("\nWhile Loop Completed.\n");

	return 0;
}