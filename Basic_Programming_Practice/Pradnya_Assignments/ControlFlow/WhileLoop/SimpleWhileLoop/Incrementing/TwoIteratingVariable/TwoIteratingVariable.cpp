#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\nPrinting Digits from 1 to 10 and 10 to 100 - ");

	i = 1;
	j = 10;
	while(i <=10, j <=100)
	{
		printf("\n%d \t %d", i, j);
		i++;
		j = j + 10;
	}

	printf("\nWhile Loop Completed.\n");

	return 0;
}