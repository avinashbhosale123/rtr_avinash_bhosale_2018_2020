#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\nPrinting Digits from 10 to 1 and 100 to 10 - ");

	i = 10;
	j = 100;
	while(i > 0, j >= 10)
	{
		printf("\n%d \t%d", i, j);
		i--;
		j = j - 10;
	}

	printf("\nWhile Loop Completed");

	return 0;
}