#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i_num, num, i;

	//Code
	printf("\nEnter an integer value from which iteration must begin: ");
	scanf("%d", &i_num);

	printf("\nHow many digits you want to printf from %d onwards: ", i_num);
	scanf("%d", &num);

	i = i_num;
	while (i <= (i_num + num))
	{
		printf("\n%d", i);
		i++;
	}

	printf("\nPrinting Completed. Bye!!\n");

	return 0;
}