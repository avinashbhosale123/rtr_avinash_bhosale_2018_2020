#include<stdio.h>
#include<conio.h>

int main(void)
{
	//Variable Declaration
	int i;
	char ch;

	printf("\nPrinting Even Numbers from 1 to 100 for every User Input. Loop exits when 'Q' or 'q' is pressed.\n");
	printf("Enter Character 'Q' or 'q' to quit the Loop: \n");

	for (i = 2; 1 <= 100; i++)
	{
		if (i % 2 == 0)
		{
			printf("\t%d\n", i);
			ch = getch();
		}
		
		if (ch == 'Q' || ch == 'q')
		{
			break;
		}
	}

	printf("\nLoop Completed.\n");

	return 0;
}