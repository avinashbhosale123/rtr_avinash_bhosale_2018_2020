#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\n");

	for (i = 1; i <= 10; i++)
	{
		for (j = 1; j <= 10; j++)
		{
			if (j > i)
			{
				break;
			}
			else
			{
				printf("* ");
			}
		}
		printf("\n");
	}

	printf("\nLoop Completed.\n");

	return 0;
}