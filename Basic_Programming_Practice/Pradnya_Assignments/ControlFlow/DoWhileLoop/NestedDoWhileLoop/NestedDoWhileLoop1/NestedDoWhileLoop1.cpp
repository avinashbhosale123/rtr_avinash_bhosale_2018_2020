#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j;

	//Code
	printf("\n");

	i = 0;
	do
	{
		printf("i = %d\n", i);
		printf("---------\n");
		j = 1;
		do
		{
			printf("\tj = %d\n", j);
			j++;
		}while (j <= 5);
		i++;
		printf("\n");
	}while (i <= 10);

	printf("Both loops end here\n");

	return 0;
}