#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i_num, num, i;

	//Code
	printf("\nEnter an integer value from which iteration must begin: ");
	scanf("%d", &i_num);

	printf("\nHow many digits you want to printf from %d onwards: ", i_num);
	scanf("%d", &num);

	i = i_num;
	do
	{
		printf("\n%d", i);
		i++;
	}while (i <= (i_num + num));

	printf("\nPrinting Completed. Bye!!\n");

	return 0;
}