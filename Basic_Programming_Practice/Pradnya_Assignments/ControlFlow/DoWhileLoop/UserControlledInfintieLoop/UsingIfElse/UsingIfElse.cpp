#include<stdio.h>
#include<conio.h>

int main(void)
{
	//Variable Declaration
	char option, ch = '\0';

	//Code
	printf("\nOnce the Infine Loop begins, Press 'Q' or 'q' to Quit the infine loop.\n");
	printf("\nPress 'Y' or 'y' to initiate Infine Loop: ");
	option = getch();

	if (option == 'Y' || option == 'y')
	{
		do
		{
			printf("\nIn Loop...");
			ch = getch();
			if (ch == 'Q' || ch == 'q')
				break; //Get out of the loop
		}while (1);
	}
	else
		printf("\nInvalid Input, You must press 'Y' or 'y' to initiate the loop!! Try Again Later\n");

	printf("\nProgram ends here!!\n");

	return 0;
}