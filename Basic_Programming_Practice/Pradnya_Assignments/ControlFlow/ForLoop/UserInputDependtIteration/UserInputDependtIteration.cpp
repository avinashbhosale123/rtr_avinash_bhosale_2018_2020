#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i_num, num, i;

	//Code
	printf("\nEnter an integer value from which iteration must begin: ");
	scanf("%d", &i_num);

	printf("\nHow many digits you can to printf from %d onwards: ", i_num);
	scanf("%d", &num);

	for (i = i_num; i <= (i_num + num); i++)
	{
		printf("\n%d", i);
	}

	printf("\nPrinting Completed. Bye!!\n");

	return 0;
}