#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i;

	//Code
	printf("\nPrinting Digits from 10 to 1 - ");

	for (i = 10; i > 0; i--)
	{
		printf("\n%d", i);
	}

	printf("\nFor Loop Completed.\n");

	return 0;
}