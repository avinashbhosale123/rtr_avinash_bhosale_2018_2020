#include<stdio.h>

int main(void)
{
	//Variable Declaration
	int i, j, k;

	//Code
	printf("\n");

	for (i = 0; i <= 10; i++)
	{
		printf("i = %d\n", i);
		printf("---------\n");
		for (j = 1; j <= 5; j++)
		{
			printf("\tj = %d\n", j);
			printf("-----------\n");
			for (k = 1; k <= 3; k++)
			{
				printf("k = %d\n", k);
			}
			printf("\n");
		}
		printf("\n");
	}

	printf("All 3 loops end here\n");

	return 0;
}