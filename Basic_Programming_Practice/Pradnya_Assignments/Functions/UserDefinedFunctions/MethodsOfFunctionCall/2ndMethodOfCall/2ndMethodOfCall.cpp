#include <stdio.h> 

int main(int argc, char *argv[], char *envp[])
{
	//Function Declarations
	void display_information(void);
	void Function_Country(void);

	//Code
	display_information(); 
	Function_Country();

	return(0);
}


void display_information(void) 
{
	//Function Declarations
	void Function_My(void);
	void Function_Name(void);
	void Function_Is(void);
	void Function_FirstName(void);
	void Function_MiddleName(void);
	void Function_Surname(void);
	void Function_OfAMC(void);

	//Code
	Function_My();
	Function_Name();
	Function_Is();
	Function_FirstName();
	Function_MiddleName();
	Function_Surname();
	Function_OfAMC();
}

void Function_My(void)
{
	//Code
	printf("\nMy");
}

void Function_Name(void)
{
	//Code
	printf("\nName");
}

void Function_Is(void)
{
	//Code
	printf("\nIs");
}

void Function_FirstName(void)
{
	//Code
	printf("\nAvinash");
}

void Function_MiddleName(void)
{
	//Code
	printf("\nDeelip");
}

void Function_Surname(void)
{
	//Code
	printf("\nBhosale");
}

void Function_OfAMC(void)
{
	//Code
	printf("\nof ASTROMEDICOMP\n");
}

void Function_Country(void)
{
	//Code
	printf("\nI live In India!!\n");
}
