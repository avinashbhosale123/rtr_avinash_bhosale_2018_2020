#include<stdio.h>

//Entry Point function with int as return value and 3 paramters
int main(int argc, char *argv[], char *envp[])
{
	//Variable declaration
	int i;

	printf("\nHello World!!\n");
	printf("Number of Command Line Arguments = %d\n", argc);
	printf("Command Line Arguments passed to this Program are:\n");

	for (i = 0; i < argc; i++)
	{
		printf("Command Line Argument Number %d = %s\n", (i + 1), argv[i]);
	}

	printf("\nFirst 20 Environmental Varibales passed to this Program are:\n");

	for (i = 0; i < 20; i++)
	{
		printf("Environmental Variable Number %d = %s\n", (i + 1), envp[i]);
	}

	return 0;
}