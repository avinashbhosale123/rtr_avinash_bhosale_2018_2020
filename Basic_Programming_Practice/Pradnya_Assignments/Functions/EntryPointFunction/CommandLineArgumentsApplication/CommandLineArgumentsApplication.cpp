#include<stdio.h>
#include<stdlib.h> //This header file contain the prototype for atoi function

int main(int argc, char *argv[], char *envp[])
{
	//Variable declaration
	int i, num, sum = 0;
	
	//Code
	printf("\nSum of All Integers Passed through Command Line Argument is: \n");

	for (i = 1; i < argc; i++)
	{
		num = atoi(argv[i]);
		sum = sum + num;
	}

	printf("Sum = %d\n", sum);

	return 0;
}