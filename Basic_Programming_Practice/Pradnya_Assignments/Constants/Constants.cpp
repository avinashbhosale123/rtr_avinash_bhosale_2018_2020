#include<stdio.h>

//defining constants using macros
#define PI 3.1415926535897932

//constants using enum

enum
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY,
};

enum
{
	JANUARY = 1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER,
};

enum numbers
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE = 5,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
};

enum boolean
{
	TRUE = 1,
	FALSE = 0,
};

int main(void)
{
	//local constant
	const double epsilon = 0.000001;

	//code
	printf("\nLocal Constant: %lf\n", epsilon);

	printf("SUNDAY is day number: %d\n", SUNDAY);
	printf("MONDAY is day number: %d\n", MONDAY);
	printf("TUESDAY is day number: %d\n", TUESDAY);
	printf("WEDNESDAY is day number: %d\n", WEDNESDAY);
	printf("THURSDAY is day number: %d\n", THURSDAY);
	printf("FRIDAY is day number: %d\n", FRIDAY);
	printf("SATURDAY is day number: %d\n\n", SATURDAY);

	printf("JANUARY is month number: %d\n", JANUARY);
	printf("FEBRUARY is month number: %d\n", FEBRUARY);
	printf("MARCH is month number: %d\n", MARCH);
	printf("APRIL is month number: %d\n", APRIL);
	printf("MAY is month number: %d\n", MAY);
	printf("JUNE is month number: %d\n", JUNE);
	printf("JULY is month number: %d\n", JULY);
	printf("AUGUST is month number:%d\n", AUGUST);
	printf("SEPTEMBER is month number: %d\n", SEPTEMBER);
	printf("OCTOBER is month number:%d\n", OCTOBER);
	printf("NOVEMBER is month number:%d\n", NOVEMBER);
	printf("DECEMBER is month number:%d\n\n", DECEMBER);

	printf("ONE is enum number: %d\n", ONE);
	printf("TWO is enum number: %d\n", TWO);
	printf("THREE is enum number: %d\n", THREE);
	printf("FOUR is enum number: %d\n", FOUR);
	printf("FIVE is enum number: %d\n", FIVE);
	printf("SIX is enum number: %d\n", SIX);
	printf("SEVEN is enum number: %d\n", SEVEN);
	printf("EIGHT is enum number: %d\n", EIGHT);
	printf("NINE is enum number: %d\n", NINE);
	printf("TEN is enum number: %d\n\n", TEN);

	printf("Value of TRUE is: %d\n", TRUE);
	printf("Value of FALSE is: %d\n\n", FALSE);

	printf("Value of PI is: %.10lf\n", PI);
	printf("AREA of circle of radius 2 units is: %f\n", (PI*2.0f*2.0f));

	return 0;
}