#include<stdio.h>

int main(void)
{
	//function declaration
	void change_count(void);

	//variable to be used as global using extern
	extern int global_count;

	//code
	printf("\nGlobal Count: %d\n", global_count);
	change_count();

	return 0;
}

int global_count = 0;

void change_count(void)
{
	global_count = global_count + 1;
	printf("Global Count: %d\n", global_count);
}