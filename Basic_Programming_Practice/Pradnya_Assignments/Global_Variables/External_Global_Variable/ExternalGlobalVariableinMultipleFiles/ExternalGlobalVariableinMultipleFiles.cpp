#include<stdio.h>

int global_count;

int main(void)
{
	//function declaration
	void change_count(void);
	void change_count_1(void);
	void change_count_2(void);

	//code
	change_count();
	change_count_1();
	change_count_2();

	return 0;
}

void change_count(void)
{
	global_count = global_count + 1;
	printf("\nGlobal Count: %d\n", global_count);
}