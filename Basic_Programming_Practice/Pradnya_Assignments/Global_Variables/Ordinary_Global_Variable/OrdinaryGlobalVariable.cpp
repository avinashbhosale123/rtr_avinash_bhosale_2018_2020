#include<stdio.h>

int global_count = 0;

int main(void)
{
	void change_count_1(void);
	void change_count_2(void);

	//code
	printf("\nGlobal Count: %d \n", global_count);

	change_count_1();
	change_count_2();

	return 0;
}

//Functions
void change_count_1(void)
{
	global_count = global_count + 1;
	printf("Global Count: %d \n", global_count);
}

void change_count_2(void)
{
	global_count = global_count + 1;
	printf("Global Count: %d \n", global_count);
}