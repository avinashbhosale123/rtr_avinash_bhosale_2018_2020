#include<stdio.h>

int main(void)
{
	//code
	printf("\nGoing on to the next line.. using \\n escape sequence \n\n");

	printf("Demonstrating \t Horizontal \t Tab \t using \\t esacpe sequence\n");

	printf("\"Double quoted output\" done using \\\" \\\" esacpe sequence\n");

	printf("\'Single quoted output\' done using \\\' \\\' escape sequence\n");

	printf("BACKSPACE turned into BACKSPACE\b using \\b escape sequence\n");

	printf("\r Demonstrating carriage return escape sequence using \\r \n");
	printf("Demonstrating \r carriage return using esacpe sequence \\r \n");
	printf("Demonstrating carriage \r return using escape sequence \\r \n");

	//0x41 is the hexadecimal code for letter 'A'
	printf("Demonstrating \x41 using \\xhh escape sequence\n");
	//\102 is the octal code for letter 'B'
	printf("Demonstrating \102 using \\ooo esacpe sequence\n");

	return 0;
}