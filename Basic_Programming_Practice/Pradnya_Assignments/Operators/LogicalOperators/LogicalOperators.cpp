#include<stdio.h>

int main(void)
{
	//variable declaration
	int a, b, c, result;

	//code
	printf("\nEnter 1st Integer: ");
	scanf("%d", &a);

	printf("\nEnter 2nd Integer: ");
	scanf("%d", &b);

	printf("\nEnter 3rd Integer: ");
	scanf("%d", &c);

	printf("\nIf the Answer is 0, it means FALSE and if the Answer is 1, it means TRUE.");

	result = (a <= b) && (b != c);
	//The result is only true in this case if both the relations are true
	printf("\nThe result of (%d <= %d) && (%d != %d) is %d", a, b, b, c, result);

	result = (b >= a) || (a == c);
	//The result is true in this case if any one of the relations is true
	printf("\nThe result of (%d >= %d) || (%d == %d) is %d", b, a, a, c, result);

	result = !a;
	printf("\nUsing Logical Not (!) operator on %d gives result %d", a, result);

	result = !b;
	printf("\nUsing Logical Not (!) operator on %d gives result %d", b, result);

	result = !c;
	printf("\nUsing Logical Not (!) operator on %d gives result %d", c, result);

	result = (!(a <= b) && !(b != c));
	printf("\nUsing Logical Not (!) operator as (!(%d <= %d) && !(%d != %d)) gives result %d", a, b, b, c, result);

	result = !((b >= a) || (a == c));
	printf("\nUsing Logical Not (!) operator as !((%d >= %d) || (%d == %d)) gives result %d", b, a, a, c, result);

	return 0;
}