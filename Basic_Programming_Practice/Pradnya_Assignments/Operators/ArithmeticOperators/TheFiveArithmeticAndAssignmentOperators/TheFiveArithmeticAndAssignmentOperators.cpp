#include<stdio.h>

int main(void)
{
	//variable declaration
	int a, b, result;

	//code
	printf("\nEnter a number: ");
	scanf("%d", &a);

	printf("\nEnter another number: ");
	scanf("%d", &b);

	result = a + b;
	printf("\nAddition of A = %d and B = %d gives result = %d\n", a, b, result);

	result = a - b;
	printf("\nSubtraction of A = %d and B = %d gives result = %d\n", a, b, result);

	result = a * b;
	printf("\nMultiplication of A = %d and B = %d gives result = %d\n", a, b, result);

	result = a / b;
	printf("\nDivision of A = %d by B = %d gives quotient = %d\n", a, b, result);

	result = a % b;
	printf("\nDivision of A = %d by B = %d gives remainder = %d\n", a, b, result);

	return 0;
}