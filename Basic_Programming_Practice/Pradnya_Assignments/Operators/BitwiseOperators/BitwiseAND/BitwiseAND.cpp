#include<stdio.h>

int main(void)
{
	//Function declaration
	void PrintBinaryFormOfNumber(unsigned int);
	
	//variable declaration;
	unsigned int a, b, result;

	//code
	printf("\nEnter an integer: ");
	scanf("%d", &a);

	printf("\nEnter another integer: ");
	scanf("%d", &b);

	result = a & b;
	printf("\n\nBitwise AND-ing of A = %d (Decimal), %o (Octal), %X (Hexadecimal) and \nB = %d (Decimal), %o (Octal), %X (Hexadecimal) \ngives the result %d (Decimal), %o (Octal), %X (Hexadecimal). \n", a, a, a, b, b, b, result, result, result);

	PrintBinaryFormOfNumber(a);
	PrintBinaryFormOfNumber(b);
	PrintBinaryFormOfNumber(result);

	return 0;
}

//Function
void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//local function varibales
	unsigned int quotient, remainder, num, binary_array[8];
	int i;

	//code
	for (i = 0; i < 8; i++)
	{
		binary_array[i] = 0;
	}

		printf("The Binary Form of the Integer %d is \t=\t", decimal_number);
		num = decimal_number;
		i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0; i < 8; i++)
	{
		printf("%u", binary_array[i]);
	}
		printf("\n");
}