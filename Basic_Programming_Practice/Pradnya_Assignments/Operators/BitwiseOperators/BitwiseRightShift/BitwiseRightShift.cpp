#include<stdio.h>

int main(void)
{
	//Function declaration
	void PrintBinaryFormOfNumber(unsigned int);

	//variable declaration;
	unsigned int a, b, result, right_shift_A, right_shift_B;

	//code
	printf("\nEnter an integer: ");
	scanf("%d", &a);

	printf("\nEnter another integer: ");
	scanf("%d", &b);

	printf("\nBy how many bits you want to Shift A = %d to the Right?: ", a);
	scanf("%u", &right_shift_A);

	printf("\nBy how many bits you want to shift B = %d to the Right?: ", b);
	scanf("%u", &right_shift_B);

	result = a >> right_shift_A;
	printf("\n\nBitwise Right Shift of %d bits of \nA = %d (Decimal), %o (Octal), %X (Hexadecimal) \ngives the result %d (Decimal), %o (Octal), %X (Hexadecimal).\n", right_shift_A, a, a, a, result, result, result);
	PrintBinaryFormOfNumber(a);
	PrintBinaryFormOfNumber(result);

	result = b >> right_shift_B;
	printf("\n\nBitwise Right Shift of %d bits of \nB = %d (Decimal), %o (Octal), %X (Hexadecimal) \ngives the result %d (Decimal), %o (Octal), %X (Hexadecimal).\n", right_shift_B, b, b, b, result, result, result);
	PrintBinaryFormOfNumber(b);
	PrintBinaryFormOfNumber(result);

	return 0;
}

//Function
void PrintBinaryFormOfNumber(unsigned int decimal_number)
{
	//local function varibales
	unsigned int quotient, remainder, num, binary_array[8];
	int i;

	//code
	for (i = 0; i < 8; i++)
	{
		binary_array[i] = 0;
	}

	printf("The Binary Form of the Integer %d is \t=\t", decimal_number);
	num = decimal_number;
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0; i < 8; i++)
	{
		printf("%u", binary_array[i]);
	}
	printf("\n");
}