#include <stdio.h>

//Macro
#define NUM_ELEMENTS 10

int main(void)
{
	//Variable Declarations 
	int iArray[NUM_ELEMENTS], i, num, sum = 0;

	//Code
	printf("\nEnter Integer Elememts for Array : \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		scanf("%d", &num);
		iArray[i] = num;
	}

	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		sum = sum + iArray[i];
	}

	printf("\nSum Of All Elements of Array = %d\n", sum);

	return 0;
}
