#include <stdio.h>

//Macro
#define NUM_ELEMENTS 10

int main(void)
{
	//Variable Declarations 
	int iArray[NUM_ELEMENTS], i, num, j, count = 0;

	//Code
	printf("\nEnter Integer Elememts for the Array: \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		scanf("%d", &num);

		//If num is negative then convert it to positive
		if (num < 0)
			num = -1 * num;

		iArray[i] = num;
	}

	//Printing Entire Array
	printf("\nArray Elements: \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		printf("%d\n", iArray[i]);
	}

	//Separating the Prime Numbers from the Array Elements
	printf("\nPrime Numbers Amongst the Array Elements are : \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		for (j = 1; j <= iArray[i]; j++)
		{
			if ((iArray[i] % j) == 0)
				count++;
		}

		if (count == 2)
		{
			printf("%d\n", iArray[i]);
		}

		count = 0;  //Reset Count for next iteration
	}

	return 0;
}

