#include <stdio.h>

//Macro
#define NUM_ELEMENTS 10

int main(void)
{
	//Variable Declarations 
	int iArray[NUM_ELEMENTS], i, num, sum = 0;

	//Code
	printf("\nEnter Integer Elememts for Array: \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		scanf("%d", &num);
		iArray[i] = num;
	}

	//Separating Even Numbers from Array Elements
	printf("\nEven Numbers Amongst the Array Elements are: \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		if ((iArray[i] % 2) == 0)
			printf("%d\n", iArray[i]);
	}

	//Separating Odd Numbers from Array Elements
	printf("\nOdd Numbers Amongst the Array Elements Are : \n");
	for (i = 0; i < NUM_ELEMENTS; i++)
	{
		if ((iArray[i] % 2) != 0)
			printf("%d\n", iArray[i]);
	}

	return 0;
}

