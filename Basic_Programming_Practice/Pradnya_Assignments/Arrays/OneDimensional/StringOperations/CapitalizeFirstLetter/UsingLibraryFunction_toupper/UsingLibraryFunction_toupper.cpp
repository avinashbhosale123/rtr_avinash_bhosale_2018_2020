#include <stdio.h>
#include <ctype.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations
	char chArray[MAX_STRING_LENGTH], chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH];
	int iStringLength, i, j;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (i == 0)
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = toupper(chArray[i]);
		}
		else if (chArray[i] == ' ')
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
			chArray_CapitalizedFirstLetterOfEveryWord[j + 1] = toupper(chArray[i + 1]);

			j++;
			i++;
		}
		else
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
		}

		j++;
	}

	chArray_CapitalizedFirstLetterOfEveryWord[j] = '\0';

	printf("\nString Entered: \n");
	printf("%s\n", chArray);

	printf("\nString after Capitalizing the First Letter of Every Word: \n");
	printf("%s\n", chArray_CapitalizedFirstLetterOfEveryWord);

	return 0;
}

int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return(string_length);
}
