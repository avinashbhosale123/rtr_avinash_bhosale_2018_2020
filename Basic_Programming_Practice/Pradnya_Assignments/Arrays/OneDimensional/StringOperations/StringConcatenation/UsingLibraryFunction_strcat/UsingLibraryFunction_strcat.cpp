#include <stdio.h>
#include<string.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable Declarations 
	char chArray_One[MAX_STRING_LENGTH], chArray_Two[MAX_STRING_LENGTH];

	//Code
	printf("\nEnter First String: \n");
	gets_s(chArray_One, MAX_STRING_LENGTH);

	printf("\nEnter Second String: \n");
	gets_s(chArray_Two, MAX_STRING_LENGTH);

	printf("\n****** BEFORE CONCATENATION ******");
	printf("\nThe Original First String Entered ('chArray_One[]'): \n");
	printf("%s\n", chArray_One);

	printf("\nThe Original Second String Entered ('chArray_Two[]'): \n");
	printf("%s\n", chArray_Two);

	strcat(chArray_One, chArray_Two);

	printf("\n****** AFTER CONCATENATION ******");
	printf("\n'chArray_One[]': \n");
	printf("%s\n", chArray_One);

	printf("\n'chArray_Two[]': \n");
	printf("%s\n", chArray_Two);

	return 0;
}
