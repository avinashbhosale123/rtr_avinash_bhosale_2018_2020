#include <stdio.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	void MyStrcpy(char[], char[]);
	
	//Variable Declarations 
	char chArray_Original[MAX_STRING_LENGTH], chArray_Copy[MAX_STRING_LENGTH];

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	MyStrcpy(chArray_Copy, chArray_Original);

	printf("\nOriginal String Entered ('chArray_Original[]'): \n");
	printf("%s\n", chArray_Original);

	printf("\nCopied String ('chArray_Copy[]'): \n");
	printf("%s\n", chArray_Copy);

	return 0;
}

void MyStrcpy(char str_destination[], char str_source[])
{
	//Variable Declarations 
	int iStringLength = 0, j;

	//Code
	iStringLength = MyStrlen(str_source);
	for (j = 0; j < iStringLength; j++)
	{
		str_destination[j] = str_source[j];
	}

	str_destination[j] = '\0';
}