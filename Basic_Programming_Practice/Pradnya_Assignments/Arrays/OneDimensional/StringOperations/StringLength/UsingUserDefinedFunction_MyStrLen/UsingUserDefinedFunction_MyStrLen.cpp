#include <stdio.h>

//Macro
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function Declaration
	int MyStrlen(char[]);

	//Variable Declarations 
	char chArray[MAX_STRING_LENGTH];
	int iStringLength = 0;

	//Code
	printf("\nEnter a String: \n");
	gets_s(chArray, MAX_STRING_LENGTH);

	printf("\nString Entered is: \n");
	printf("%s\n", chArray);

	iStringLength = MyStrlen(chArray);
	printf("\nLength of the String Is = %d Characters !!\n", iStringLength);

	return 0;
}

//MyStrLen
int MyStrlen(char str[])
{
	//Variable Declarations
	int j, string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
		{
			break;
		}
		else
		{
			string_length++;
		}
	}

	return(string_length);
}
