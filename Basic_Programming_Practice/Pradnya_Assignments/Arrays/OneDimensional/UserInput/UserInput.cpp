#include<stdio.h>
#include<conio.h>

//Macros constants for defining the size of Array
#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 3
#define CHAR_ARRAY_NUM_ELEMENTS 15

int main(void)
{
	//varibale Declarations
	int iArray[INT_ARRAY_NUM_ELEMENTS];
	float fArray[FLOAT_ARRAY_NUM_ELEMENTS];
	char cArray[CHAR_ARRAY_NUM_ELEMENTS];
	int i;

	//Code
	//User Input for Array Elements
	printf("\nEnter Elements for 'Integer' Array:\n");
	for (i = 0; i < INT_ARRAY_NUM_ELEMENTS; i++)
	{
		scanf("%d", &iArray[i]);
	}

	printf("\nEnter Elements for 'Float' Array:\n");
	for (i = 0; i < FLOAT_ARRAY_NUM_ELEMENTS; i++)
	{
		scanf("%f", &fArray[i]);
	}

	printf("\nEnter Elements for 'Character' Array:\n");
	for (i = 0; i < CHAR_ARRAY_NUM_ELEMENTS; i++)
	{
		cArray[i] = getch();
		printf("%c\n", cArray[i]);
	}

	//Printing Array Elements
	printf("\n'Integer' Array Elements entered by You:\n");
	for (i = 0; i < INT_ARRAY_NUM_ELEMENTS; i++)
	{
		printf("%d\n", iArray[i]);
	}

	printf("\n'Float' Array Elements enetered by You:\n");
	for (i = 0; i < FLOAT_ARRAY_NUM_ELEMENTS; i++)
	{
		printf("%f\n", fArray[i]);
	}

	printf("\n'Character' Array Elements entered by You:\n");
	for (i = 0; i < CHAR_ARRAY_NUM_ELEMENTS; i++)
	{
		printf("%c\n", cArray[i]);
	}

	return 0;
}