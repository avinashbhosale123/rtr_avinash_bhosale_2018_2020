#include <stdio.h>

int main(void)
{
	//Variable Declarations
	int iArray[3][5],int_size, iArray_size, iArray_num_elements, iArray_num_rows, iArray_num_columns;
	int i, j;

	//Code
	int_size = sizeof(int);

	iArray_size = sizeof(iArray);
	printf("\nSize of the Two Dimensional Integer Array = %d\n", iArray_size);

	iArray_num_rows = iArray_size / sizeof(iArray[0]);
	printf("\nNumber of Rows in the Two Dimensional Integer Array = %d\n", iArray_num_rows);

	iArray_num_columns = sizeof(iArray[0]) / int_size;
	printf("\nNumber of Columns in the Two Dimensional Integer Array = %d\n", iArray_num_columns);

	iArray_num_elements = iArray_num_rows * iArray_num_columns;
	printf("\nNumber of Elements in the Two Dimensional Integer Array = %d\n", iArray_num_elements);

	printf("\nElements in the 2D Array: \n");

	//Row 1
	iArray[0][0] = 21;
	iArray[0][1] = 42;
	iArray[0][2] = 63;
	iArray[0][3] = 84;
	iArray[0][4] = 105;

	//Row 2
	iArray[1][0] = 22;
	iArray[1][1] = 44;
	iArray[1][2] = 66;
	iArray[1][3] = 88;
	iArray[1][4] = 110;

	//Row 3
	iArray[2][0] = 23;
	iArray[2][1] = 46;
	iArray[2][2] = 69;
	iArray[2][3] = 92;
	iArray[2][4] = 115;

	for (i = 0; i < iArray_num_rows; i++)
	{
		printf("\n****** ROW %d ******\n", (i + 1));
		for (j = 0; j < iArray_num_columns; j++)
		{
			printf("iArray[%d][%d] = %d\n", i, j, iArray[i][j]);
		}
	}

	return 0;
}

